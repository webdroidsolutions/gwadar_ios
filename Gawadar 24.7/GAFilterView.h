//
//  GAFilterView.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 28/10/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAFilterView : UIView
@property (strong, nonatomic) IBOutlet UITextField *layer1Field;
@property (strong, nonatomic) IBOutlet UIButton *layer1Button;
@property (strong, nonatomic) IBOutlet UITextField *layer2Field;
@property (strong, nonatomic) IBOutlet UIButton *layer2Button;
@property (strong, nonatomic) IBOutlet UITextField *areaFromField;
@property (strong, nonatomic) IBOutlet UITextField *areaToField;
@property (strong, nonatomic) IBOutlet UITextField *priceFromField;
@property (strong, nonatomic) IBOutlet UITextField *priceToField;
@property (strong, nonatomic) IBOutlet UIButton *filterButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UITextField *sortByField;
@property (strong, nonatomic) IBOutlet UIButton *sortByButton;

@end
