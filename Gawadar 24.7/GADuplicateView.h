//
//  GADuplicateView.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 13/01/2018.
//  Copyright © 2018 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GADuplicateView : UIView
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
