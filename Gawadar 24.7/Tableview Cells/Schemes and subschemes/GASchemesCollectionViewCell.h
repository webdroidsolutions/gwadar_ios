//
//  GASchemesCollectionViewCell.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 24/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GASchemesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *propertyImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
