//
//  GAHomeTableViewCell.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 27/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAHomeTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *propertyImageView;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *areaLabel;
@property (strong, nonatomic) IBOutlet UILabel *schemeLabel;
@property (strong, nonatomic) IBOutlet UILabel *sizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *subschemeLabel;
@property (strong, nonatomic) IBOutlet UILabel *phaseLabel;
@property (strong, nonatomic) IBOutlet UIButton *addToFavrtButton;
@property (strong, nonatomic) IBOutlet UIButton *sendInquiryButton;
@property (strong, nonatomic) IBOutlet UILabel *lastLabel;
@property (strong, nonatomic) IBOutlet UIImageView *typeImageView;


@property (strong, nonatomic) IBOutlet UIImageView *icon1;
@property (strong, nonatomic) IBOutlet UIImageView *icon2;
@property (strong, nonatomic) IBOutlet UIImageView *icon3;
@property (strong, nonatomic) IBOutlet UIImageView *icon4;
@property (strong, nonatomic) IBOutlet UIImageView *icon5;
@property (strong, nonatomic) IBOutlet UIImageView *icon6;

@property (strong, nonatomic) IBOutlet UILabel *line1;
@property (strong, nonatomic) IBOutlet UILabel *line2;
@property (strong, nonatomic) IBOutlet UILabel *line3;
@property (strong, nonatomic) IBOutlet UILabel *line4;
@property (strong, nonatomic) IBOutlet UILabel *line5;
@property (strong, nonatomic) IBOutlet UILabel *line6;


@end
