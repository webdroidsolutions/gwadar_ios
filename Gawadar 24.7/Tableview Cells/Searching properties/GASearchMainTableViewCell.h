//
//  GASearchMainTableViewCell.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 5/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GASearchMainTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *checkImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
