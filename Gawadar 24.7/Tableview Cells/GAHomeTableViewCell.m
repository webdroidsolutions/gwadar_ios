//
//  GAHomeTableViewCell.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 27/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAHomeTableViewCell.h"

@implementation GAHomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.priceLabel.frame = CGRectMake(self.priceLabel.frame.origin.x, self.priceLabel.frame.origin.y - 5, self.priceLabel.frame.size.width, self.priceLabel.frame.size.height + 10);
    self.priceLabel.numberOfLines = 0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.contentView.frame = UIEdgeInsetsInsetRect(self.contentView.frame, UIEdgeInsetsMake(10, 0, 0, 0));
}

@end
