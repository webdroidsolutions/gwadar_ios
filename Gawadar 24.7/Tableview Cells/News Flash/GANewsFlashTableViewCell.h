//
//  GANewsFlashTableViewCell.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/10/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GANewsFlashTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIImageView *newsImageView;

@end
