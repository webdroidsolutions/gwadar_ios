//
//  GAConstants.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Colors

#define GAThemeColor [UIColor colorWithRed:15/255.0f green:82/255.0f blue:51/255.0f alpha:1.0f]

#define GAOddCellColor [UIColor whiteColor]
#define GAEvenCellColor [UIColor colorWithRed:242/255.0f green:242/255.0f blue:242/255.0f alpha:1.0]


#pragma mark - Fonts

#define GASmallFont [UIFont fontWithName:@"HelveticaNeue" size:12.0f]
#define GAMediumFont [UIFont fontWithName:@"HelveticaNeue" size:15.0f]
#define GALargeFont  [UIFont fontWithName:@"HelveticaNeue" size:18.0f]

#pragma mark - User defaults keys

#define GALoginInformationDictionaryKey @"GALoginInformationDictionaryKey"

#define GALoginInfoLoginIdKey @"GALoginInfoLoginIdKey"
#define GALoginInfoLoginTokenKey @"GALoginInfoLoginTokenKey"
#define GALoginInfoFullNameKey @"GALoginInfoFullNameKey"
#define GALoginInfoPhoneKey @"GALoginInfoPhoneKey"
#define GALoginInfoCnicKey @"GALoginInfoCnicKey"
#define GALoginInfoOrganizationKey @"GALoginInfoOrganizationKey"
#define GALoginInfoAdressKey @"GALoginInfoAdressKey"


//FCM Token
#define GAFCMTokenKey @"GAFCMTokenKey"

#pragma mark - Web Service URL 

#define GABaseURL @"http://www.gwadar247.pk/api/"

// login
#define GALoginServiceURL [GABaseURL stringByAppendingString:@"login.php"]

// create Account
#define GACreateAccountServiceURL [GABaseURL stringByAppendingString:@"signup.php"]


// news feeds
#define GANewsFeedsServiceURL [GABaseURL stringByAppendingString:@"newsfeed2.php"]

// send iquiry
#define GASendInquiryServiceURL [GABaseURL stringByAppendingString:@"sendrequest.php"]
#define GACancelInquiryServiceURL [GABaseURL stringByAppendingString:@"deleterequest.php"]

// favourite
#define GAFavouritePropertiesServiceURL [GABaseURL stringByAppendingString:@"favoriteproperties2.php"]
#define GAAddToFavouriteServiceURL [GABaseURL stringByAppendingString:@"addfavorite.php"]
#define GADeleteFromFavouriteServiceURL [GABaseURL stringByAppendingString:@"deletefavorite.php"]

// add property
#define GASchemeListServiceURL [GABaseURL stringByAppendingString:@"schemes.php"]
#define GASubSchemeListServiceURL [GABaseURL stringByAppendingString:@"subschemes.php"]
#define GADivision1ServiceURL [GABaseURL stringByAppendingString:@"layer1.php"]
#define GADivision2ServiceURL [GABaseURL stringByAppendingString:@"layer2.php"]
#define GAPlotListServiceURL [GABaseURL stringByAppendingString:@"plots.php"]
#define GAPlotAreaTypeServiceURL [GABaseURL stringByAppendingString:@"plotareatype.php"]
#define GAAddPropertyServiceURL [GABaseURL stringByAppendingString:@"addproperty.php"]
#define GARoadListServiceURL [GABaseURL stringByAppendingString:@"roads.php"]
#define GAAddOpenLandPropertyServiceURL [GABaseURL stringByAppendingString:@"addpropopenland.php"]
#define GAGetDupicatesServiceURL [GABaseURL stringByAppendingString:@"getduplicates.php"]
#define GAGetSchemeMapServiceURL [GABaseURL stringByAppendingString:@"getsubschememap.php"]


// my properties
#define GAMyPropertiesServiceURL [GABaseURL stringByAppendingString:@"myproperties2.php"]

#define GARemovePropertyServiceURL [GABaseURL stringByAppendingString:@"deleteproperty.php"]

// notifications
#define GANotificationsListServiceURL [GABaseURL stringByAppendingString:@"notifications2.php"]
#define GARespondToNotificationServiceURL [GABaseURL stringByAppendingString:@"respondrequest.php"]

// Search
#define GASearchListServiceURL [GABaseURL stringByAppendingString:@"searchsubchemelist.php"]
#define GASearchResultServiceURL [GABaseURL stringByAppendingString:@"search2.php"]
#define GAFilterSearchServiceURL [GABaseURL stringByAppendingString:@"filter2.php"]

// news flash
#define GANewsFlashServiceURL [GABaseURL stringByAppendingString:@"newsflash.php"]

// maps
#define GAMapPinsServiceURL [GABaseURL stringByAppendingString:@"gwadarmappins.php"]
#define GAMapsListServiceURL [GABaseURL stringByAppendingString:@"districtmaps.php"]
#define GASearchByPinServiceURL [GABaseURL stringByAppendingString:@"searchbypin2.php"]
#define GAMasterPlanServiceURL [GABaseURL stringByAppendingString:@"masterplan.php"]
#define GADistrictMapServiceURL [GABaseURL stringByAppendingString:@"districtmap.php"]

// contanct
#define GAContactAdminServiceURL [GABaseURL stringByAppendingString:@"getcontact.php"]

// my request
#define GAMyRequestServiceURL [GABaseURL stringByAppendingString:@"myrequests2.php"]



@interface GAConstants : NSObject

@end
