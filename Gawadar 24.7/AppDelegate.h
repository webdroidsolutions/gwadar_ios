//
//  AppDelegate.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

/*
 
 #pragma mark - Web service methods
 
 -(void) callService
 {
 MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 
 NSString *serviceURL = @"";
 
 NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
 
 NSDictionary *params = @{
 @"" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
 @"" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
 };
 
 GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
 
 [manager POST:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
 
 NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
 
 if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
 {
 
 
 [hud hideAnimated:YES];
 }
 else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
 {
 GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@""];
 
 [ISMessages showCardAlertWithTitle:@"Session Expired" message:@"Session Expired . Please Login Again" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
 
 UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
 window.rootViewController = loginVC;
 }
 else
 {
 [hud hideAnimated:YES];
 [ISMessages showCardAlertWithTitle:@"" message:@"" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
 }
 
 
 } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
 
 [hud hideAnimated:YES];
 [ISMessages showCardAlertWithTitle:@"" message:@"" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
 
 }];
 
 
 }
*/ 
 


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

