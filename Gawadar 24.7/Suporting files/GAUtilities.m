//
//  GAUtilities.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAUtilities.h"

@implementation GAUtilities

+(float)getPercentSizeWithParentSize:(float)parentSize percent:(float)percent
{
    return parentSize*percent/100;
}

+(NSDictionary *)getDictionaryFromData:(NSData *)data
{
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    return json;
}

@end
