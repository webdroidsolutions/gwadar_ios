//
//  GAWebServiceSingleton.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 23/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"


@interface GAWebServiceSingleton : AFHTTPSessionManager

+(instancetype) getInstance;

@end
