//
//  GAUtilities.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GAUtilities : NSObject

+(float) getPercentSizeWithParentSize : (float) parentSize percent : (float) percent;

+(NSDictionary *) getDictionaryFromData : (NSData *) data;

@end
