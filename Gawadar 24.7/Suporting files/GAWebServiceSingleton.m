//
//  GAWebServiceSingleton.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 23/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAWebServiceSingleton.h"


@implementation GAWebServiceSingleton

static GAWebServiceSingleton *manager;
-(instancetype) init {
    
    if(!manager)
    {
        manager = [AFHTTPSessionManager manager];
        //   self.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
    }
    
    return manager;
}


+(GAWebServiceSingleton *)getInstance {
    
    if(!manager)
        manager = [[GAWebServiceSingleton alloc] init];
    return manager;
}


@end
