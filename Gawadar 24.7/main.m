//
//  main.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
