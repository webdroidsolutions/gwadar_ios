//
//  GAHomeScreenNavigationViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 19/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAHomeScreenNavigationViewController.h"
#import "GAHomeScreenViewController.h"
#import "GASearchPropertiesViewController.h"
#import "GASideMenuViewController.h"

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"

#import "GAConstants.h"

@interface GAHomeScreenNavigationViewController ()
{
    LGSideMenuController *sideMenuController;
    
    BOOL isMenuShown;
}
@end

@implementation GAHomeScreenNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationBar.barTintColor = GAThemeColor;
    self.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationBar.translucent = NO;
    
    self.navigationItem.title = @"News Feed";

    
    // bar button item
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(didTapSearchItem)];
    
    self.navigationItem.rightBarButtonItem = searchItem;
    
    
    
    
    GAHomeScreenViewController *homeScreenViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAHomeScreenViewController"];
    
    GASideMenuViewController *sideController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GASideMenuViewController"];
    
     sideMenuController = [[LGSideMenuController alloc] initWithRootViewController:homeScreenViewController leftViewController:sideController rightViewController:nil];
    ;
    sideMenuController.leftViewWidth = self.view.frame.size.width/2;


    [self initWithRootViewController:sideMenuController];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - event handlers

-(void) didTapSideMenuButton
{
    
    if(!isMenuShown)
    {
        [sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
    else
    {
        [sideMenuController hideLeftViewAnimated:NO];
    }
     isMenuShown = !isMenuShown;
}

-(void) didTapSearchItem
{
    GASearchPropertiesViewController  *searchController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GASearchPropertiesViewController"];
    [self.navigationController pushViewController:searchController animated:YES];
}

@end
