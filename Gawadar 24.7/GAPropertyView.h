//
//  GAPropertyView.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAPropertyView : UIView
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UILabel *label3;
@property (strong, nonatomic) IBOutlet UILabel *label4;
@property (strong, nonatomic) IBOutlet UILabel *label5;
@property (strong, nonatomic) IBOutlet UILabel *label6;
@property (strong, nonatomic) IBOutlet UIButton *upperButton;
@property (strong, nonatomic) IBOutlet UIButton *lowerButton;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (strong, nonatomic) IBOutlet UILabel *upperLabel;
@property (strong, nonatomic) IBOutlet UILabel *lowerLabel;
@property (strong, nonatomic) IBOutlet UIImageView *responseImageView;
@property (strong, nonatomic) IBOutlet UIImageView *icon1;
@property (strong, nonatomic) IBOutlet UIImageView *icon2;
@property (strong, nonatomic) IBOutlet UIImageView *icon3;
@property (strong, nonatomic) IBOutlet UIImageView *icon6;
@property (strong, nonatomic) IBOutlet UIImageView *icon4;
@property (strong, nonatomic) IBOutlet UIImageView *icon5;

@end
