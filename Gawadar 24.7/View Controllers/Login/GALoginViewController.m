//
//  GALoginViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/7/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GALoginViewController.h"



#define LogoImageViewHeight 100
#define LogoImageViewWidth 100
#define SigninButtonWidth 200

@interface GALoginViewController ()
{
   
}
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UIButton *signinButton;
@property (strong, nonatomic) IBOutlet UIView *usernameView;
@property (strong, nonatomic) IBOutlet UIView *passwordView;

@end

@implementation GALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // username view
    self.usernameView.layer.cornerRadius = 5.0f;
    self.usernameView.clipsToBounds = YES;
    
    //password view
    self.passwordView.layer.cornerRadius = 5.0f;
    self.passwordView.clipsToBounds = YES;

    // sign in button
    self.signinButton.layer.cornerRadius = 5.0f;
    self.signinButton.clipsToBounds = YES;

    
    self.usernameView.layer.borderColor = GAThemeColor.CGColor;
    self.usernameView.layer.borderWidth = 0.5;
    
    self.passwordView.layer.borderWidth = 0.5f;
    self.passwordView.layer.borderColor = GAThemeColor.CGColor;
    
    
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBActions

- (IBAction)didTapSigninButton:(id)sender {
    
    [self callService];
}

- (IBAction)didTapForgotPasswordButton:(id)sender {
}
- (IBAction)didTapCreateAccountButton:(id)sender {

    UIViewController *createAccountViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GACreateAccountViewController"];

    [self presentViewController:createAccountViewController animated:YES completion:nil];
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GALoginServiceURL;
    
    NSString *fcmToken = [[NSUserDefaults standardUserDefaults] objectForKey:GAFCMTokenKey];
    
    NSDictionary *params = @{
                             @"phone" : _usernameField.text,
                             @"password" : _passwordField.text,
                             @"fcmToken" : fcmToken? fcmToken : @""
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager POST:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
       
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            NSDictionary *loginInfo = @{
                                        GALoginInfoLoginIdKey : [json objectForKey:@"UserID"],
                                          GALoginInfoLoginTokenKey : [json objectForKey:@"Token"],
                                        GALoginInfoFullNameKey : [json objectForKey:@"Name"],
                                        GALoginInfoPhoneKey : [json objectForKey:@"Phone"],
                                        GALoginInfoCnicKey : [json objectForKey:@"CNIC"],
                                        GALoginInfoAdressKey : [json objectForKey:@"Address"],
                                        GALoginInfoOrganizationKey : [json objectForKey:@"Organization"],
                                        };
            
            [[NSUserDefaults standardUserDefaults] setObject:loginInfo forKey:GALoginInformationDictionaryKey];
            
            [self performSegueWithIdentifier:@"Login" sender:nil];
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"001"])
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Invalid username or password" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if([[json objectForKey:@"ErrorCode"] isEqualToString:@"900"])
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Login Failed. Account Verification Pending" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Login" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in login. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
    
}
- (IBAction)contactAdmin:(id)sender {
    
    [self contact];
}

-(void) contact
{
    NSString *serviceURL = GAContactAdminServiceURL;
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager POST:serviceURL parameters:@{} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([json objectForKey:@"Phone"])
        {
            NSString *phone = [json objectForKey:@"Phone"];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phone]]];
        }
        else
        {
            [ISMessages showCardAlertWithTitle:@"ErrorQ" message:@"Error in contacting admin. Try Again Later" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
       
            [ISMessages showCardAlertWithTitle:@"ErrorQ" message:@"Error in contacting admin. Try Again Later" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
        
    }];
    
}

@end
