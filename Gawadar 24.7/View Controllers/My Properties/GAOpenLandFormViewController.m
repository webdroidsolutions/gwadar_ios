//
//  GAOpenLandFormViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 17/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAOpenLandFormViewController.h"

#import "ActionSheetPicker.h"


@interface GAOpenLandFormViewController () <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>
{
    UITableView *tableView;
    
    UISegmentedControl *segmentControl;
    
    UITextField *priceField;
    UITextField *costField;
    UITextField *areaField;
    
    UITextField *roadFrontField;
    UITextField *seaFrontField;
    UITextField *road1NameField;
    UITextField *road1AcreField;
    UITextField *road2NameField;
    UITextField *road2AcreField;
    UITextField *pricePerAcreField;

    
    
    UIButton *submitButton;
    UIButton *roadFrontButton;
    UIButton *seaFrontButton;
    UIButton *road1NameButton;
    UIButton *road2NameButton;
    
    UILabel *commisionLabel;
    UILabel *totalPriceLabel;

    NSMutableArray *roadNameArray;
    
    NSArray *seafrontArray;
    NSArray *roadFrontArray;
    
    NSString *roadFront;
    NSString *seaFront;
    NSString *road1Name;
    NSString *road1Acre;
    NSString *road2Name;
    NSString *road2Acre;
    NSString *pricePerAcre;

    int totalRows;
    int roadFrontCount;
}
@end

@implementation GAOpenLandFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    seafrontArray = @[@"0",@"1",@"2",@"3"];
    roadFrontArray = @[@"0",@"1",@"2"];
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-49)];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.navigationItem.title = @"Add Plot Details";

    totalRows = 4;

    // segment control
    segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Sale",@"Wanted"]];
    segmentControl.tintColor = GAThemeColor;
    segmentControl.selectedSegmentIndex = 0;
    [segmentControl addTarget:self action:@selector(didChangeSegmentControlValue) forControlEvents:UIControlEventValueChanged];

    priceField = [self configureTextField];
    costField = [self configureTextField];
    areaField = [self configureTextField];
    seaFrontField = [self configureTextField];
    roadFrontField = [self configureTextField];
    road1NameField = [self configureTextField];
    road1AcreField = [self configureTextField];
    road2NameField = [self configureTextField];
    road2AcreField = [self configureTextField];
    pricePerAcreField = [self configureTextField];
    
    
    roadFrontButton = [self configureButton];
    seaFrontButton = [self configureButton];
    road1NameButton = [self configureButton];
    road2NameButton = [self configureButton];
    
    commisionLabel = [[UILabel alloc] init];
    commisionLabel.font = GASmallFont;
    
    totalPriceLabel = [[UILabel alloc] init];
    totalPriceLabel.font = GASmallFont;
    
    submitButton = [[UIButton alloc] init];
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton setBackgroundColor:GAThemeColor];
    submitButton.layer.cornerRadius = 10.0f;
    submitButton.titleLabel.font = GASmallFont;
    [submitButton setFrame:CGRectMake(self.view.frame.size.width/2 - (self.view.frame.size.width/4), 10, self.view.frame.size.width/2, 40)];
    [submitButton addTarget:self action:@selector(didTapSubmitButton) forControlEvents:UIControlEventTouchUpInside];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    // keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];\
    
    [self.view addSubview:tableView];
    
    [priceField addTarget:self action:@selector(priceFieldTextDidChange) forControlEvents:UIControlEventEditingChanged];


    UIBarButtonItem *mapItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"district.png"] style:UIBarButtonItemStyleDone target:self action:@selector(didTapMapItem)];
    self.navigationItem.rightBarButtonItem = mapItem;
    
    [self getRoadNames];

    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    areaField.inputAccessoryView = numberToolbar;
    
    priceField.inputAccessoryView = numberToolbar;
    road1AcreField.inputAccessoryView = numberToolbar;
    road2AcreField.inputAccessoryView = numberToolbar;
    pricePerAcreField.inputAccessoryView = numberToolbar;

}

#pragma mark - Return key on number pad
-(void)cancelNumberPad{
    
    UITextField *numberTextField = nil;
    
    if ([areaField isFirstResponder])
    {
        numberTextField = areaField;
    }
    else if ([priceField isFirstResponder])
    {
        numberTextField = priceField;
    }
    else
    {
        return;
    }
    
    [numberTextField resignFirstResponder];
    numberTextField.text = @"";
}

-(void)doneWithNumberPad{
    
    UITextField *numberTextField = nil;
    
    if ([areaField isFirstResponder])
    {
        numberTextField = areaField;
    }
    else if ([priceField isFirstResponder])
    {
        numberTextField = priceField;
    }
    else if ([road1AcreField isFirstResponder])
    {
        numberTextField = road1AcreField;
    }
    else if ([road2AcreField isFirstResponder])
    {
        numberTextField = road2AcreField;
    }
    else if ([pricePerAcreField isFirstResponder])
    {
        numberTextField = pricePerAcreField;
    }
    else
    {
        return;
    }
    [numberTextField resignFirstResponder];
}

#pragma mark - web service methods

-(void) getRoadNames
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GARoadListServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            roadNameArray = [[NSMutableArray alloc] init];
            
            NSArray *tempArray = [json objectForKey:@"Roads"];
            
            for (NSDictionary *dict in tempArray)
            {
                [roadNameArray addObject:[dict objectForKey:@"Name"]];
            }
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

-(void) addProperty
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@?userid=%@&token=%@",GAAddOpenLandPropertyServiceURL,[loginInfo objectForKey:GALoginInfoLoginIdKey],[loginInfo objectForKey:GALoginInfoLoginTokenKey]];
    
    NSMutableDictionary *params = @{
                                    @"schemeid" : self.schemeID,
                                    @"subschemeid" : self.subSchemeID,
                                    @"propertytype" : (segmentControl.selectedSegmentIndex == 0)? @"Sale" : @"Wanted",
                                    @"seafront":seaFrontField.text,
                                    @"roadfront":roadFrontField.text,
                                    @"road1name" : road1NameField.text,
                                    @"road1acre":road1AcreField.text,
                                    @"road2name":road2NameField.text,
                                    @"road2acre":road2AcreField.text,
                                    @"priceacre":pricePerAcreField.text,
                                    @"price":totalPriceLabel.text
                                    }.mutableCopy;
    
    

    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager POST:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property Added successfully" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@""];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired" message:@"Session Expired . Please Login Again" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
            window.rootViewController = loginVC;
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Adding Property" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Adding Property. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];
}


#pragma mark - configure fields and button
-(UIButton *) configureButton
{
    UIButton *button = [[UIButton alloc] init];
    button.imageView.image = [UIImage imageNamed:@"dropdown.png"];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [button addTarget:self action:@selector(didTapDropDownMenuButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

-(UITextField *) configureTextField
{
    UITextField *field = [[UITextField alloc] init];
    field.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    field.borderStyle = UITextBorderStyleNone;
    field.delegate = self;
    field.keyboardType = UIKeyboardTypeDecimalPad;
    
    return field;
}

#pragma mark - tableView cell view methods

-(void)  getCellViewWithDropDownButtonWithSuperView : (UIView *) superView TextField : (UITextField *) textField DropDownButton : (UIButton *) dropDownButton buttonTag : (int) tag LabelText : (NSString *) labelText placeHolder : (NSString *) placeHolder
{
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:25], superView.frame.size.height)];
    
    titleLabel.text = labelText;
    titleLabel.font = GASmallFont;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [superView addSubview:titleLabel];
    
    
    
    UIView *verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake((titleLabel.frame.origin.x+titleLabel.frame.size.width+2), 2 , 1, superView.frame.size.height-4)];
    
    [verticalSeparatorView setBackgroundColor:[UIColor blackColor]];
    [superView addSubview:verticalSeparatorView];
    
    
    UIView *horizontalSeapratorView = [[UIView alloc] initWithFrame:CGRectMake(10, superView.frame.size.height-2, self.view.frame.size.width-20, 1)];
    horizontalSeapratorView.backgroundColor = [UIColor blackColor];
    [superView addSubview:horizontalSeapratorView];
    
    
    
    [textField setFrame:CGRectMake(verticalSeparatorView.frame.origin.x+verticalSeparatorView.frame.size.width + 3, verticalSeparatorView.frame.origin.y, self.view.frame.size.width - verticalSeparatorView.frame.origin.x - 15, verticalSeparatorView.frame.size.height)];
    textField.borderStyle = UITextBorderStyleNone;
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.placeholder = placeHolder;
    textField.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    textField.userInteractionEnabled = NO;
    
    [superView addSubview: textField];
    
    [dropDownButton setFrame:textField.frame];
    [dropDownButton  setImage:[UIImage imageNamed:@"dropdown.png"] forState:UIControlStateNormal];
    dropDownButton.tag = tag;
    
    [superView addSubview:dropDownButton];
    
    
}


-(void) getCellViewWithTextFieldWithSuperview : (UIView *) superView  textField :(UITextField *) textField LabelText :(NSString *) labelText PlaceHolder : (NSString *) placeHolder
{
    UILabel *titleLabel;
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:25], superView.frame.size.height)];
    
    titleLabel.text = labelText;
    titleLabel.font = GASmallFont;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
   if(labelText.length > 12)
   {
       titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.0f];
   }
    
    [superView addSubview:titleLabel];
    
    
    UIView *verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake((titleLabel.frame.origin.x+titleLabel.frame.size.width+2), 2 , 1, superView.frame.size.height-4)];
    
    [verticalSeparatorView setBackgroundColor:[UIColor blackColor]];
    [superView addSubview:verticalSeparatorView];
    
    
    UIView *horizontalSeapratorView = [[UIView alloc] initWithFrame:CGRectMake(10, superView.frame.size.height-2, self.view.frame.size.width-20, 1)];
    horizontalSeapratorView.backgroundColor = [UIColor blackColor];
    [superView addSubview:horizontalSeapratorView];
    
    
    [textField setFrame:CGRectMake(verticalSeparatorView.frame.origin.x+verticalSeparatorView.frame.size.width + 3, verticalSeparatorView.frame.origin.y, self.view.frame.size.width - verticalSeparatorView.frame.origin.x - 4, verticalSeparatorView.frame.size.height)];
    textField.placeholder = placeHolder;
    
    
    [superView addSubview:textField];
}

#pragma mark - event handlers

-(void) didChangeSegmentControlValue
{

    roadFrontCount = 0;
    [tableView reloadData];
//    if (segmentControl.selectedSegmentIndex == 0)
//    {
//        [tableView beginUpdates];
//
//        NSIndexSet *set = [NSIndexSet indexSetWithIndex:1];
//
//        [tableView insertSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
//        [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:7 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
//
//
//        [tableView endUpdates];
//
//    }
//    else if (segmentControl.selectedSegmentIndex == 1)
//    {
//        [tableView beginUpdates];
//
//        NSIndexSet *set = [NSIndexSet indexSetWithIndex:1];
//
//        [tableView deleteSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
//        [tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:7 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
//
//        [tableView endUpdates];
//    }

}

-(void) didTapDropDownMenuButton : (id) sender
{
    UIButton *tappedButton = (UIButton *) sender;

    NSArray *rowsArray;

    switch (tappedButton.tag) {
    
        case 0:
            rowsArray = seafrontArray;
            break;
            
        case 1:
            rowsArray = roadFrontArray;
            break;
            
        case 2:
        case 3:
            rowsArray = roadNameArray;
            break;
    
    }
    
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select your option" rows:rowsArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        
        switch (tappedButton.tag) {
            case 0:
                seaFrontField.text = [seafrontArray objectAtIndex:selectedIndex];
                break;
                
            case 1:
                roadFrontField.text = [roadFrontArray objectAtIndex:selectedIndex];

                roadFrontCount = [roadFrontField.text longLongValue];

                road1NameField.text = @"";
                road1AcreField.text = @"";
                road2NameField.text = @"";
                road2AcreField.text = @"";

                [tableView reloadData];

                break;
                
            case 2:
                road1NameField.text = [roadNameArray objectAtIndex:selectedIndex];
                break;
                
            case 3:
                road2NameField.text = [roadNameArray objectAtIndex:selectedIndex];
                break;
                
            default:
                break;
        }
        
    } cancelBlock:nil origin:self.view];
    
    
    
    
}

-(void) didTapSubmitButton
{
    
    if([roadFrontField.text isEqualToString:@""]||[seaFrontField.text isEqualToString:@""]||[road1NameField.text isEqualToString:@""])
    {
        [ISMessages showCardAlertWithTitle:@"Error!" message:@"Fill All require fields" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        return;
    }

    [self addProperty];

}

-(void) didTapMapItem
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSString *serviceURL = GAGetSchemeMapServiceURL;


    NSDictionary *params = @{
                             @"subschemeid" : self.subSchemeID
                             };

    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];

    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];

        [hud hideAnimated:YES];

        if([json objectForKey:@"Image"])
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[json objectForKey:@"Image"]]];
        else
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading Map Image." iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];


    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading Map Image. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];

}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(segmentControl.selectedSegmentIndex == 0)
        return 3;
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
//        return segmentControl.selectedSegmentIndex == 0? 8:7;
        return totalRows + (roadFrontCount * 2);
    }
    
    if(section == 1 && segmentControl.selectedSegmentIndex == 0)
        return 4;
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.section == 0)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                [segmentControl setFrame:CGRectMake(5, 2, self.view.frame.size.width-10, cell.contentView.frame.size.height-4)];
                [cell.contentView addSubview:segmentControl];
            }
                break;
                
            case 1:
                
//                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:seaFrontField DropDownButton:seaFrontButton buttonTag:0 LabelText:@"Sea Front" placeHolder:@"Select Sea Front"];

                if (segmentControl.selectedSegmentIndex == 0)
                [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:areaField LabelText:@"Area" PlaceHolder:@"Enter Area"];
                else if (segmentControl.selectedSegmentIndex == 1)
                {

                }

                break;
                
            case 2:
                
                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:roadFrontField DropDownButton:roadFrontButton buttonTag:1 LabelText:@"Road Front" placeHolder:@"Select Road Front"];
                
                break;
                
            case 3:
                
//                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:road1NameField DropDownButton:road1NameButton buttonTag:2 LabelText:@"Road 1 Name" placeHolder:@"Select Road 1 Name"];
                if(roadFrontCount == 0)
                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:seaFrontField DropDownButton:seaFrontButton buttonTag:0 LabelText:@"Sea Front" placeHolder:@"Select Sea Front"];
                else
                    [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:road1NameField DropDownButton:road1NameButton buttonTag:2 LabelText:@"Road 1 Name" placeHolder:@"Select Road 1 Name"];

                break;
                
            case 4:

                 [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:road1AcreField LabelText:@"Road 1 Acre" PlaceHolder:@"Enter Road 1 Acre"];
               // [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:road1AcreField LabelText:@"Road 1 Acre" PlaceHolder:@"Enter Road 1 Acre"];

                break;
                
            case 5:
                
//                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:road2NameField DropDownButton:road2NameButton buttonTag:3 LabelText:@"Road 2 Name" placeHolder:@"Select Road 2 Name"];
                if(roadFrontCount == 1)
                    [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:seaFrontField DropDownButton:seaFrontButton buttonTag:0 LabelText:@"Sea Front" placeHolder:@"Select Sea Front"];
                else
                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:road2NameField DropDownButton:road2NameButton buttonTag:3 LabelText:@"Road 2 Name" placeHolder:@"Select Road 2 Name"];
                break;
                
            case 6:
//                [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:road2AcreField LabelText:@"Road 2 Acre" PlaceHolder:@"Enter Road 2 Acre"];

        [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:road2AcreField LabelText:@"Road 2 Acre" PlaceHolder:@"Enter Road 2 Acre"];

                break;
                
            case 7:
//                [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:pricePerAcreField LabelText:@"Price Per Acre" PlaceHolder:@"Enter Price Per Acre"];

    [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:seaFrontField DropDownButton:seaFrontButton buttonTag:0 LabelText:@"Sea Front" placeHolder:@"Select Sea Front"];
                break;
        }
    }
    if (indexPath.section == 1)
    {
        
        switch (indexPath.row) {

            case 0:
                if(segmentControl.selectedSegmentIndex == 1)
                {
                    [cell.contentView addSubview:submitButton];
                    break;
                }

                [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:pricePerAcreField LabelText:@"Price Per Acre" PlaceHolder:@"Enter Price Per Acre"];
                break;

            case 1:

                
                [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:priceField LabelText:@"Your Price" PlaceHolder:@"Enter Price"];
                
                break;
                
            case 2:
            {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:40], cell.frame.size.height)];
                titleLabel.text = @"Commission (1%): ";
                titleLabel.font = GASmallFont;
                [cell.contentView addSubview: titleLabel];
                
                
                [commisionLabel setFrame:CGRectMake(titleLabel.frame.size.width, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:75], cell.frame.size.height)];
                [cell.contentView addSubview:commisionLabel];
                commisionLabel.text = @"-";
                
                
            }
                break;
                
            case 3:
            {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:40], cell.frame.size.height)];
                titleLabel.text = @"Total Price : ";
                titleLabel.font = GASmallFont;
                [cell.contentView addSubview: titleLabel];
                
                
                [totalPriceLabel setFrame:CGRectMake(titleLabel.frame.size.width, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:75], cell.frame.size.height)];
                [cell.contentView addSubview:totalPriceLabel];
                totalPriceLabel.text = @"-";
                
            }
                
            default:
                break;
        }
        
    }
    
    if(indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
                
                [cell.contentView addSubview:submitButton];
                
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (segmentControl.selectedSegmentIndex == 1 && indexPath.row == 1)
        return 0;

    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 1 && segmentControl.selectedSegmentIndex == 0)
        return 30;
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0 || section == 2)
    {
        return nil;
    }

    if (segmentControl.selectedSegmentIndex == 1)
        return nil;

    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = GAThemeColor;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-5, 30)];
    titleLabel.font = GASmallFont;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"Price";
    [headerView addSubview:titleLabel];
    
    
    return headerView;
}

#pragma mark - UITextField Delegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];

    return YES;
}

#pragma mark - Keyboard methods
-(void) keyboardWillShow :(NSNotification *) notification{
    
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [tableView setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-216-64)];
    
    
    
    
}

-(void) keyboardWillHide :(NSNotification *) notification{
    
    [tableView setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-49)];
}

-(void) priceFieldTextDidChange
{
    float value = [priceField.text floatValue];
    
    float commision = [GAUtilities getPercentSizeWithParentSize:value percent:1];
    
    commisionLabel.text = [NSString stringWithFormat:@"%f",commision];
    totalPriceLabel.text = [NSString stringWithFormat:@"%f",(value+commision)];
    
}
@end
