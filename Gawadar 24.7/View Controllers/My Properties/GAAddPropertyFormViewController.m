//  GAAddPropertyFormViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 24/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAAddPropertyFormViewController.h"

#import "ActionSheetPicker.h"

#import "CNPPopupController.h"

#import "GADuplicateView.h"


@interface GAAddPropertyFormViewController () <UITableViewDelegate , UITableViewDataSource,UITextFieldDelegate>
{
    UISegmentedControl *segmentControl;
    
    UIButton *schemeButton;
    UIButton *streetButton;
    UIButton *plotButton;
    UIButton *residentialButton;
    UIButton *commercialButton;
    UIButton *areaButton;
    UIButton *submitButton;
    UIButton *TypeButton;
    
    UITextField *schemeField;
    UITextField *streetField;
    UITextField *plotField;
    UITextField *areaField;
    UITextField *priceField;
    UITextField *typeField;
    
    UILabel *typeLabel;
    UILabel *commisionLabel;
    UILabel *totalPriceLabel;
    
    NSMutableDictionary *schemeListDictionary;
    NSMutableDictionary *layerListDictionary;
    NSMutableDictionary *plotListDictionary;
    NSMutableDictionary *areaListDictionary;
    NSMutableDictionary *typeListDictionary;
    NSMutableDictionary *plotAreaDictionary;
    NSMutableDictionary *areaDictionary;
    
    NSString *layer1ID;
    NSString *layer2ID;
    NSString *plotID;
    NSString *layer2Name;
    NSString *area;
    NSString *type;
    
    NSArray *division1List;
    NSArray *plotList;
    NSArray *duplicateList;
    
    CNPPopupController *popupController;
    
    BOOL shouldShowStreet;
    
    int totalRows;
    
    GADuplicateView * popupView;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation GAAddPropertyFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationItem.title = @"Plot Details";
    
    [self.tableView setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-49)];
    
    shouldShowStreet = NO;
    
    // segment control
    segmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Sale",@"Wanted"]];
    segmentControl.tintColor = GAThemeColor;
    segmentControl.selectedSegmentIndex = 0;
    [segmentControl addTarget:self action:@selector(didChangeSegmentControlValue) forControlEvents:UIControlEventValueChanged];
    
    
    // type buttons
    
    residentialButton = [self configureTypeButtonWithTitle:@"Residential"];
    commercialButton = [self configureTypeButtonWithTitle:@"Commercial"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    schemeButton = [self configureButton];
    streetButton = [self configureButton];
    plotButton = [self configureButton];
    areaButton = [self configureButton];
    TypeButton = [self configureButton];
    
    
    schemeField = [self configureTextField];
    streetField = [self configureTextField];
    plotField = [self configureTextField];
    areaField = [self configureTextField];
    priceField = [self configureTextField];
    typeField = [self configureTextField];

    typeField.userInteractionEnabled = NO;
    
    priceField.keyboardType = UIKeyboardTypeDecimalPad;
    areaField.keyboardType = UIKeyboardTypeDecimalPad;
    
    [priceField addTarget:self action:@selector(priceFieldTextDidChange) forControlEvents:UIControlEventEditingChanged];
    
    commisionLabel = [[UILabel alloc] init];
    commisionLabel.font = GASmallFont;
    
    totalPriceLabel = [[UILabel alloc] init];
    totalPriceLabel.font = GASmallFont;
    
    submitButton = [[UIButton alloc] init];
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton setBackgroundColor:GAThemeColor];
    submitButton.layer.cornerRadius = 10.0f;
    submitButton.titleLabel.font = GASmallFont;
    [submitButton setFrame:CGRectMake(self.view.frame.size.width/2 - (self.view.frame.size.width/4), 10, self.view.frame.size.width/2, 40)];
    [submitButton addTarget:self action:@selector(didTapSubmitButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self getFirstLayer];
    
    // keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    commercialButton.userInteractionEnabled = NO;
    residentialButton.userInteractionEnabled = NO;

    totalRows = 5;


    UIBarButtonItem *mapItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"district.png"] style:UIBarButtonItemStyleDone target:self action:@selector(didTapMapItem)];
    self.navigationItem.rightBarButtonItem = mapItem;
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    areaField.inputAccessoryView = numberToolbar;

    priceField.inputAccessoryView = numberToolbar;
    
}

#pragma mark - Return key on number pad
-(void)cancelNumberPad{
    
    UITextField *numberTextField = nil;
    
    if ([areaField isFirstResponder])
    {
        numberTextField = areaField;
    }
    else if ([priceField isFirstResponder])
    {
        numberTextField = priceField;
    }
    else
    {
        return;
    }
    
    [numberTextField resignFirstResponder];
    numberTextField.text = @"";
}

-(void)doneWithNumberPad{
    
    UITextField *numberTextField = nil;
    
    if ([areaField isFirstResponder])
    {
        numberTextField = areaField;
    }
    else if ([priceField isFirstResponder])
    {
        numberTextField = priceField;
    }
    else
    {
        return;
    }
    [numberTextField resignFirstResponder];
}

#pragma mark - Web service methods

-(void) getFirstLayer
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GADivision1ServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"subschemeid" : self.ID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            
            schemeListDictionary = [[NSMutableDictionary alloc] init];
            division1List = [json objectForKey:@"Layers"];
            
            for (NSDictionary *dict in division1List) {
                
                [schemeListDictionary setObject:[dict objectForKey:@"ID"] forKey:[dict objectForKey:@"Name"]];
            }
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];
    
}

-(void) getSecondLayer
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GADivision2ServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"layer1id" : layer1ID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            
            layerListDictionary = [[NSMutableDictionary alloc] init];
            NSArray * division2List = [json objectForKey:@"Layers"];
            
            for (NSDictionary *dict in division2List) {
                
                [layerListDictionary setObject:[dict objectForKey:@"ID"] forKey:[dict objectForKey:@"Name"]];
            }
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];
    
}

-(void) getPlots
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GAPlotListServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSMutableDictionary *params = @{
                                    @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                                    @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]                             }.mutableCopy;
    
    if(shouldShowStreet)
    {
        [params setObject:layer2ID forKey:@"layer2id"];
    }
    else
    {
        [params setObject:layer1ID forKey:@"layer1id"];
    }
    
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            
            plotListDictionary = [[NSMutableDictionary alloc] init];
            plotList = [json objectForKey:@"Plots"];
            
            for (NSDictionary *dict in plotList) {
                
                [plotListDictionary setObject:[dict objectForKey:@"ID"] forKey:[dict objectForKey:@"Name"]];
            }
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@""];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired" message:@"Session Expired . Please Login Again" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
            window.rootViewController = loginVC;
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];
    
}

-(void) getPlotAreaType
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GAPlotAreaTypeServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSMutableDictionary *params = @{
                                    @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                                    @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]                             }.mutableCopy;
    
    if(shouldShowStreet)
    {
        [params setObject:layer2ID forKey:@"layer2id"];
    }
    else
    {
        [params setObject:layer1ID forKey:@"layer1id"];
    }
    
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            
            areaListDictionary = [[NSMutableDictionary alloc] init];
            typeListDictionary = [[NSMutableDictionary alloc] init];
            
            plotAreaDictionary = [[NSMutableDictionary alloc] init];
            
            plotList = [json objectForKey:@"Areas"];
            
            for (NSDictionary *dict in plotList) {
                
                [plotAreaDictionary setObject:[dict objectForKey:@"Area"] forKey:[dict objectForKey:@"Area"]];
            }
            
            NSArray *typeList = [json objectForKey:@"Types"];
            
            areaDictionary = [[NSMutableDictionary alloc] init];
            
            for (NSDictionary *dict in typeList)
            {
                [areaDictionary setObject:[dict objectForKey:@"Type"] forKey:[dict objectForKey:@"Type"]];
            }
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@""];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired" message:@"Session Expired . Please Login Again" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
            window.rootViewController = loginVC;
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading the list. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];
}

-(void) addProperty
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@?userid=%@&token=%@",GAAddPropertyServiceURL,[loginInfo objectForKey:GALoginInfoLoginIdKey],[loginInfo objectForKey:GALoginInfoLoginTokenKey]];
    
    NSMutableDictionary *params = @{
                                    @"plotid" : plotID,
                                    @"area" : area,
                                    @"propertytype" : (segmentControl.selectedSegmentIndex == 0)? @"Sale" : @"Wanted"
                                    }.mutableCopy;
    
    if(segmentControl.selectedSegmentIndex == 0)
    {
        [params setObject:totalPriceLabel.text forKey:@"price"];
        [params setObject:area forKey:@"area"];
    }
    else
    {
        [params setObject:layer1ID forKey:@"layer1id"];
        
        if(shouldShowStreet)
        [params setObject:layer2ID forKey:@"layer2id"];
        
        [params setObject:areaField.text forKey:@"wantedpropertyarea"];
        [params setObject:typeField.text forKey:@"wantedpropertytype"];
      
        
        [params setObject:self.ID forKey:@"subschemeid"];
        [params setObject:self.SchemeID forKey:@"schemeid"];
        
    }
    
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager POST:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property Added successfully" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@""];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired" message:@"Session Expired . Please Login Again" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
            window.rootViewController = loginVC;
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Adding Property" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Adding Property. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];
}

#pragma mark - configure fields and button

-(UIButton *) configureTypeButtonWithTitle : (NSString *) title
{
    UIButton *button = [[UIButton alloc] init];
    
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button setTitleColor:GAThemeColor forState:UIControlStateNormal];
    button.titleLabel.font = GASmallFont;
    
    [button setImage:nil forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"check_white.png"] forState:UIControlStateSelected];
    
    [button addTarget:self action:@selector(didTapTypeButton:) forControlEvents:UIControlEventTouchUpInside];
    
    button.layer.borderColor = GAThemeColor.CGColor;
    button.layer.borderWidth = 1.0f;
    button.layer.cornerRadius = 15.0f;
    
    return button;
}

-(UIButton *) configureButton
{
    UIButton *button = [[UIButton alloc] init];
    button.imageView.image = [UIImage imageNamed:@"dropdown.png"];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [button addTarget:self action:@selector(didTapDropDownMenuButton:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

-(UITextField *) configureTextField
{
    UITextField *field = [[UITextField alloc] init];
    field.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    field.borderStyle = UITextBorderStyleNone;
    field.delegate = self;

    return field;
}

#pragma mark - tableView cell view methods

-(void)  getCellViewWithDropDownButtonWithSuperView : (UIView *) superView TextField : (UITextField *) textField DropDownButton : (UIButton *) dropDownButton buttonTag : (int) tag LabelText : (NSString *) labelText placeHolder : (NSString *) placeHolder
{
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [GAUtilities getPercentSizeWithParentSize:superView.frame.size.width percent:25], superView.frame.size.height)];
    
    titleLabel.text = labelText;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [superView addSubview:titleLabel];
    
    
    
    UIView *verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake((titleLabel.frame.origin.x+titleLabel.frame.size.width+2), 2 , 1, superView.frame.size.height-4)];
    
    [verticalSeparatorView setBackgroundColor:[UIColor blackColor]];
    [superView addSubview:verticalSeparatorView];
    
    
    UIView *horizontalSeapratorView = [[UIView alloc] initWithFrame:CGRectMake(10, superView.frame.size.height-2, self.view.frame.size.width-20, 1)];
    horizontalSeapratorView.backgroundColor = [UIColor blackColor];
    [superView addSubview:horizontalSeapratorView];
    
    
    
    [textField setFrame:CGRectMake(verticalSeparatorView.frame.origin.x+verticalSeparatorView.frame.size.width + 3, verticalSeparatorView.frame.origin.y, self.view.frame.size.width - verticalSeparatorView.frame.origin.x - 15, verticalSeparatorView.frame.size.height)];
    textField.borderStyle = UITextBorderStyleNone;
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.placeholder = placeHolder;
    textField.font = [UIFont fontWithName:@"HelveticaNeue" size:12.0f];
    textField.userInteractionEnabled = NO;
    
    [superView addSubview: textField];
    
    [dropDownButton setFrame:textField.frame];
    [dropDownButton  setImage:[UIImage imageNamed:@"dropdown.png"] forState:UIControlStateNormal];
    [dropDownButton addTarget:self action:@selector(didTapDropDownMenuButton:) forControlEvents:UIControlEventTouchUpInside];
    dropDownButton.tag = tag;
    
    [superView addSubview:dropDownButton];
    
    
}


-(void) getCellViewWithTextFieldWithSuperview : (UIView *) superView  textField :(UITextField *) textField LabelText :(NSString *) labelText PlaceHolder : (NSString *) placeHolder
{
    UILabel *titleLabel;
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [GAUtilities getPercentSizeWithParentSize:superView.frame.size.width percent:25], superView.frame.size.height)];
    
    titleLabel.text = labelText;
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0f];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [superView addSubview:titleLabel];
    
    
    UIView *verticalSeparatorView = [[UIView alloc] initWithFrame:CGRectMake((titleLabel.frame.origin.x+titleLabel.frame.size.width+2), 2 , 1, superView.frame.size.height-4)];
    
    [verticalSeparatorView setBackgroundColor:[UIColor blackColor]];
    [superView addSubview:verticalSeparatorView];
    
    
    UIView *horizontalSeapratorView = [[UIView alloc] initWithFrame:CGRectMake(10, superView.frame.size.height-2, self.view.frame.size.width-20, 1)];
    horizontalSeapratorView.backgroundColor = [UIColor blackColor];
    [superView addSubview:horizontalSeapratorView];
    
    
    [textField setFrame:CGRectMake(verticalSeparatorView.frame.origin.x+verticalSeparatorView.frame.size.width + 3, verticalSeparatorView.frame.origin.y, self.view.frame.size.width - verticalSeparatorView.frame.origin.x - 4, verticalSeparatorView.frame.size.height)];
    textField.placeholder = placeHolder;
    
    
    [superView addSubview:textField];
}



#pragma mark - event handlers

-(void) didChangeSegmentControlValue
{
    schemeField.text = @"";
    streetField.text = @"";
    plotField.text = @"";
    typeField.text = @"";
    areaField.text = @"";
    priceField.text = @"";
    
    [commercialButton setSelected:NO];
    [residentialButton setSelected:NO];
 
    [commercialButton setBackgroundColor:[UIColor whiteColor]];
    commercialButton.layer.borderWidth = 1.0f;

    residentialButton.layer.borderWidth = 1.0f;
    [residentialButton setBackgroundColor:[UIColor whiteColor]];
    
    if (segmentControl.selectedSegmentIndex == 0)
    {
        [self.tableView beginUpdates];
        
        NSIndexSet *set = [NSIndexSet indexSetWithIndex:1];
        
        [self.tableView insertSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self.tableView endUpdates];
        
        areaField.userInteractionEnabled = NO;
        [residentialButton setTitle:@"Residential" forState:UIControlStateNormal];
        [commercialButton setTitle:@"Commercial" forState:UIControlStateNormal];
        
        commercialButton.userInteractionEnabled = NO;
        residentialButton.userInteractionEnabled = NO;
        
        totalRows = 5;
    }
    else if (segmentControl.selectedSegmentIndex == 1)
    {
        [self.tableView beginUpdates];
        
        NSIndexSet *set = [NSIndexSet indexSetWithIndex:1];
        
        [self.tableView deleteSections:set withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self.tableView endUpdates];
        
        areaField.userInteractionEnabled = YES;
        [residentialButton setTitle:@"Commercial" forState:UIControlStateNormal];
        [commercialButton setTitle:@"Any" forState:UIControlStateNormal];
        
        commercialButton.userInteractionEnabled = YES;
        residentialButton.userInteractionEnabled = YES;
 
        totalRows = 4;
    
    }
    
    shouldShowStreet = NO;
    double delayInSeconds = 0.2;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self.tableView reloadData];
    });
    
}

-(void) didTapTypeButton : (id) sender
{
    
    UIButton *senderButton = (UIButton *) sender;
    
    if([senderButton isEqual:residentialButton])
    {
        [residentialButton setSelected:YES];
        [commercialButton setSelected:NO];
        
        [residentialButton setBackgroundColor:GAThemeColor];
        [commercialButton setBackgroundColor:[UIColor whiteColor]];
        
        residentialButton.layer.borderWidth = 0.0f;
        commercialButton.layer.borderWidth = 1.0f;
    }
    else if ([senderButton isEqual:commercialButton])
    {
        [residentialButton setSelected:NO];
        [commercialButton setSelected:YES];
        
        residentialButton.layer.borderWidth = 1.0f;
        commercialButton.layer.borderWidth = 0.0f;
        
        [commercialButton setBackgroundColor:GAThemeColor];
        [residentialButton setBackgroundColor:[UIColor whiteColor]];
        
    }
    
}

-(void) didTapSubmitButton
{
    if([plotField.text isEqualToString:@""]||[areaField.text isEqualToString:@""])
    {
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Fill All require field" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
        return;
    }
    
    
    [self addProperty];
}

-(void) didTapDropDownMenuButton : (id) sender
{
    UIButton *tappedButton = (UIButton *) sender;
    NSArray *rowsArray;
    switch (tappedButton.tag) {
            
        case 0:
            rowsArray = schemeListDictionary.allKeys;
            break;
            
        case 1:
            if(!schemeListDictionary){
                
                [ISMessages showCardAlertWithTitle:@"Warning!" message:@"Select above options First!" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeWarning alertPosition:ISAlertPositionTop];
                return;
            }
            rowsArray = layerListDictionary.allKeys;
            break;
            
        case 2 :
            if(((shouldShowStreet) && (!layerListDictionary)) || ((!shouldShowStreet) && (!schemeListDictionary)))
            {
                
                [ISMessages showCardAlertWithTitle:@"Warning!" message:@"Select Above options first" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeWarning alertPosition:ISAlertPositionTop];
                return;
            }
            if(segmentControl.selectedSegmentIndex == 0)
                rowsArray = plotListDictionary.allKeys;
            else
                rowsArray = plotAreaDictionary.allKeys;
            
            break;
            
        case 3:
        {
            if(((shouldShowStreet) && (!layerListDictionary)) || ((!shouldShowStreet) && (!schemeListDictionary)))
            {
                
                [ISMessages showCardAlertWithTitle:@"Warning!" message:@"Select Above options first" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeWarning alertPosition:ISAlertPositionTop];
                return;
            }
            
            rowsArray = areaDictionary.allKeys;
        }
            break;
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
    rowsArray=[rowsArray sortedArrayUsingDescriptors:@[sort]];

    if (!rowsArray || rowsArray.count == 0)
    {
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Fill Above Information First" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

        return;
    }

    [ActionSheetStringPicker showPickerWithTitle:@"Select From below options" rows:rowsArray initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        switch (tappedButton.tag)
        {
            case 0:
            {

                streetField.text = @"";
                plotField.text = @"";
                areaField.text = @"";
                priceField.text = @"";
                typeField.text = @"";

                layer2ID = nil;
                plotID = nil;

                schemeField.text = [rowsArray objectAtIndex:selectedIndex];
                layer1ID = [NSString stringWithFormat:@"%@",[schemeListDictionary objectForKey:selectedValue]];
                
                NSDictionary *currentScheme ;
                
                for (NSDictionary *dict in division1List)
                {
                    if([[NSString stringWithFormat:@"%@",[dict objectForKey:@"ID"]] isEqualToString:layer1ID])
                    {
                        currentScheme = dict;
                        break;
                    }
                }
                
                BOOL streetFlag = [[currentScheme valueForKey:@"HasLayer"] boolValue];
                
                if(streetFlag && !shouldShowStreet)
                {
                    totalRows ++;
                    shouldShowStreet = streetFlag;
                    layer2Name = [currentScheme objectForKey:@"LayerName"];
                    
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                    
                    [self getSecondLayer];
                }
                else if (!streetFlag && shouldShowStreet)
                {
                    totalRows --;
                    shouldShowStreet = streetFlag;
                    
                    [self.tableView beginUpdates];
                    [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
                    [self.tableView endUpdates];
                }
                
                if(streetFlag)
                    [self getSecondLayer];
                else
                {
                    if(segmentControl.selectedSegmentIndex == 0)
                        [self getPlots];
                    else
                        [self getPlotAreaType];
                    
                }
                streetField.text = @"";
            }
                break;
                
            case 1:
            {

                plotField.text = @"";
                areaField.text = @"";
                priceField.text = @"";
                typeField.text = @"";

                plotID = nil;

                streetField.text = [rowsArray objectAtIndex:selectedIndex];
                layer2ID = [NSString stringWithFormat:@"%@",[layerListDictionary objectForKey:selectedValue]];
               
                if(segmentControl.selectedSegmentIndex == 0)
                    [self getPlots];
                else
                    [self getPlotAreaType];
                
            }
                break;
                
            case 2:
            {

                areaField.text = @"";
                priceField.text = @"";
                typeField.text = @"";

                plotField.text = [rowsArray objectAtIndex:selectedIndex];
                
                if(segmentControl.selectedSegmentIndex == 0)
                {
                    plotID = [NSString stringWithFormat:@"%@",[plotListDictionary objectForKey:selectedValue]];
                    NSDictionary *currentPlot ;
                    
                    for (NSDictionary *dict in plotList)
                    {
                        if([[NSString stringWithFormat:@"%@",[dict objectForKey:@"ID"]] isEqualToString:plotID])
                        {
                            currentPlot = dict;
                            
                           
                            break;
                        }
                    }
                    
//                    if([[currentPlot objectForKey:@"Type"] isEqualToString:@"Residential"])
//                    {
//                        [self didTapTypeButton:residentialButton];
//                    }
//                    else if ([[currentPlot objectForKey:@"Type"] isEqualToString:@"Commercial"])
//                    {
//                        [self didTapTypeButton:commercialButton];
//                    }
//                    else
//                    {
//                        [commercialButton setTitle:[currentPlot objectForKey:@"Type"] forState:UIControlStateSelected];
//                        [self didTapTypeButton:commercialButton];
//                    }

                    typeField.text = [currentPlot objectForKey:@"Type"];

                    areaField.text = [NSString stringWithFormat:@"%@ %@",[currentPlot objectForKey:@"Area"],self.areaUnit];
                    area = [currentPlot objectForKey:@"Area"];
                    
                    if([[currentPlot objectForKey:@"HasDuplicate"] isEqualToString:@"true"])
                    {
                        [self showDuplicatesWithPlotID:[currentPlot objectForKey:@"ID"] plotName:[rowsArray objectAtIndex:selectedIndex]];
                        return;
                    }
                  
                }
                else
                {
                    areaField.text = [rowsArray objectAtIndex:selectedIndex];
                    plotID = [NSString stringWithFormat:@"%@",[plotAreaDictionary objectForKey:selectedValue]];
                }
            }
                break;
        
            case 3:
            {

                typeField.text = [rowsArray objectAtIndex:selectedIndex];
                
                area = [NSString stringWithFormat:@"%@",[areaDictionary objectForKey:selectedValue]];
            }
                break;
        }
        
        
        
    } cancelBlock:nil origin:self.view];
}


-(void) showDuplicatesWithPlotID : (NSString *) ID plotName : (NSString *) plotName
{
    
    GADuplicateView *popupView = [[NSBundle mainBundle] loadNibNamed:@"DuplicateView" owner:self options:nil][0];
//    popupView.tableView
//    popupView.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 200, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.height percent:60]) style:UITableViewStylePlain];
    popupView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    popupView.tableView.delegate = self;
    popupView.tableView.dataSource = self;
    
    popupController = [[CNPPopupController alloc] initWithContents:@[popupView]];
    
    popupController.theme = [CNPPopupTheme defaultTheme];
    
    CNPPopupTheme *theme = [[CNPPopupTheme alloc] init];
    
    theme.presentationStyle = CNPPopupPresentationStyleFadeIn;
    theme.shouldDismissOnBackgroundTouch = NO;
    theme.backgroundColor = [UIColor whiteColor];
    theme.popupStyle = CNPPopupStyleCentered;
    theme.maskType = CNPPopupMaskTypeDimmed;
    theme.maxPopupWidth = self.view.frame.size.width-10;
    theme.dismissesOppositeDirection = YES;
    theme.animationDuration = 0.5f;
    theme.contentVerticalPadding = 5.0f;
    theme.cornerRadius = 10.0f;
    theme.movesAboveKeyboard = NO;
    
    
    popupController.theme = theme;
    
    
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];

        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];

        window.rootViewController = loginVC;
    }

    NSString *serviceURL = GAGetDupicatesServiceURL;

    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];

    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"plotid" : plotID
                             };


    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];

    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];

        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            duplicateList = [json objectForKey:@"Plots"];
            double delayInSeconds = 0.5;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [popupController presentPopupControllerAnimated:YES];

            });

        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];

            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }

        else
        {
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

    }];

}

-(void) didTapMapItem
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSString *serviceURL = GAGetSchemeMapServiceURL;


    NSDictionary *params = @{
                             @"subschemeid" : self.ID
                             };

    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];

    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];

            [hud hideAnimated:YES];

        if([json objectForKey:@"Image"])
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[json objectForKey:@"Image"]]];
        else
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading Map Image." iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];


    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading Map Image. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
    }];

}

#pragma mark - tableview delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    if([tableView.superview isKindOfClass: [GADuplicateView class]])
    {
        return 1;
    }
    
    if(segmentControl.selectedSegmentIndex == 0)
        return 3;
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if([tableView.superview isKindOfClass: [GADuplicateView class]])
    {
        return duplicateList.count;
    }
    
    if (section == 0)
    {
        return totalRows;
    }
    
    if(section == 1 && segmentControl.selectedSegmentIndex == 0)
        return 3;
    
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 
   if([tableView.superview isKindOfClass: [GADuplicateView class]])
    {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 3, tableView.frame.size.width - 10, 40)];
        titleLabel.backgroundColor = [UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.layer.borderColor = GAThemeColor.CGColor;
        titleLabel.layer.borderWidth = 2.0f;
        titleLabel.clipsToBounds = YES;
        titleLabel.font = GASmallFont;
        
        
        NSDictionary *currentPlot = [duplicateList objectAtIndex:indexPath.row];
        titleLabel.text = [NSString stringWithFormat:@"  %@ %@",[currentPlot objectForKey:@"Area"],[currentPlot objectForKey:@"Type"]];
        
        [cell.contentView addSubview:titleLabel];
        
        
        return cell;
        
    }
    
    if(indexPath.section == 0)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                [segmentControl setFrame:CGRectMake(5, 2, self.view.frame.size.width-10, cell.contentView.frame.size.height-4)];
                [cell.contentView addSubview:segmentControl];
            }
                break;
                
            case 1:
            {
                [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:schemeField DropDownButton:schemeButton buttonTag:0 LabelText:self.layerName placeHolder:[NSString stringWithFormat:@"Select %@",self.layerName]];
            }
                break;
                
            case 2:
            {
                if(shouldShowStreet)
                {
                    [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:streetField DropDownButton:streetButton buttonTag:1 LabelText:layer2Name placeHolder:[NSString stringWithFormat:@"Select %@",layer2Name]];
                }
                else
                {
                    if(segmentControl.selectedSegmentIndex == 0)
                    [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:plotField DropDownButton:plotButton buttonTag:2 LabelText:@"Plot" placeHolder:@"Select plot"];
                    
                    else
                    [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:areaField DropDownButton:plotButton buttonTag:2 LabelText:@"Area" placeHolder:@"Select plot area"];
                
                }
                
            }
                break;
                
            case 3:
            {
                
                if(shouldShowStreet)
                {
                    if(segmentControl.selectedSegmentIndex == 0)
                        [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:plotField DropDownButton:plotButton buttonTag:2 LabelText:@"Plot" placeHolder:@"Select plot"];
                    
                    else
                        [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:areaField DropDownButton:plotButton buttonTag:2 LabelText:@"Plot Area" placeHolder:@"Select plot area"];
                }
                else
                {
                    if(segmentControl.selectedSegmentIndex == 0 )
                    {
//                    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:25], cell.frame.size.height)];
//                    titleLabel.text = @"Type";
//                    titleLabel.font = GASmallFont;
//                    titleLabel.textAlignment = NSTextAlignmentCenter;
//                    
//                    
//                    [cell.contentView addSubview: titleLabel];
//                    
//                    
//                    UIView *buttonsView = [[UIView alloc] initWithFrame:CGRectMake(titleLabel.frame.size.width , 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:75], titleLabel.frame.size.height)];
//                    
//                    
//                    [residentialButton setFrame:CGRectMake(0,0, [GAUtilities getPercentSizeWithParentSize:buttonsView.frame.size.width percent:40], buttonsView.frame.size.height)];
//                    
//                    [commercialButton setFrame:CGRectMake([GAUtilities getPercentSizeWithParentSize:buttonsView.frame.size.width percent:50], 0, residentialButton.frame.size.width, residentialButton.frame.size.height)];
//                    
//                    [buttonsView addSubview:residentialButton];
//                    [buttonsView addSubview:commercialButton];
//                    
//                    [cell.contentView addSubview:buttonsView];

                        [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:typeField LabelText:@"Type" PlaceHolder:@"Type of plot"];

                    }
                    else
                    {
                        [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:typeField DropDownButton:TypeButton buttonTag:3 LabelText:@"Type" placeHolder:@"Select Type"];
                    }
                    
                }
            }
                break;
                
                
                
            case 4:
            {
                if(shouldShowStreet)
                {
                    if(segmentControl.selectedSegmentIndex == 0 )
                    {
                        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:25], cell.frame.size.height)];
                        titleLabel.text = @"Type";
                        titleLabel.font = GASmallFont;
                        titleLabel.textAlignment = NSTextAlignmentCenter;
                        
                        
                        [cell.contentView addSubview: titleLabel];
                        
                        
                        UIView *buttonsView = [[UIView alloc] initWithFrame:CGRectMake(titleLabel.frame.size.width , 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:75], titleLabel.frame.size.height)];
                        
                        
                        [residentialButton setFrame:CGRectMake(0,0, [GAUtilities getPercentSizeWithParentSize:buttonsView.frame.size.width percent:40], buttonsView.frame.size.height)];
                        
                        [commercialButton setFrame:CGRectMake([GAUtilities getPercentSizeWithParentSize:buttonsView.frame.size.width percent:50], 0, residentialButton.frame.size.width, residentialButton.frame.size.height)];
                        
                        [buttonsView addSubview:residentialButton];
                        [buttonsView addSubview:commercialButton];
                        
                        [cell.contentView addSubview:buttonsView];
                        
                    }
                    else
                    {
                        [self getCellViewWithDropDownButtonWithSuperView:cell.contentView TextField:typeField DropDownButton:TypeButton buttonTag:3 LabelText:@"Type" placeHolder:@"Select Type"];
                    }
                    
                }
                else
                {
                    
                    [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:areaField LabelText:@"Area" PlaceHolder:@"Area"];
                }
                
                
            }
                break;
                
            case 5:
            {
                    [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:areaField LabelText:@"Area" PlaceHolder:@"Area"];            }
                break;
        }
        
    }
    
    
    if (indexPath.section == 1)
    {
        
        switch (indexPath.row) {
            case 0:
                
                if(segmentControl.selectedSegmentIndex == 1)
                {
                    [cell.contentView addSubview:submitButton];
                    break;
                }
                
                [self getCellViewWithTextFieldWithSuperview:cell.contentView textField:priceField LabelText:@"Your Price" PlaceHolder:@"Enter Price"];
                
                break;
                
            case 1:
            {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:40], cell.frame.size.height)];
                titleLabel.text = @"Commission (1%): ";
                titleLabel.font = GASmallFont;
                [cell.contentView addSubview: titleLabel];
                
                
                [commisionLabel setFrame:CGRectMake(titleLabel.frame.size.width, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:75], cell.frame.size.height)];
                [cell.contentView addSubview:commisionLabel];
                commisionLabel.text = @"-";
                
                
            }
                break;
                
            case 2:
            {
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:40], cell.frame.size.height)];
                titleLabel.text = @"Total Price : ";
                titleLabel.font = GASmallFont;
                [cell.contentView addSubview: titleLabel];
                
                
                [totalPriceLabel setFrame:CGRectMake(titleLabel.frame.size.width, 0, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:75], cell.frame.size.height)];
                [cell.contentView addSubview:totalPriceLabel];
                totalPriceLabel.text = @"-";
                
            }
                
            default:
                break;
        }
        
    }
    
    if(indexPath.section == 2)
    {
        switch (indexPath.row) {
            case 0:
             
                [cell.contentView addSubview:submitButton];
                
                break;
                
            default:
                break;
        }
    }
    
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView.superview isKindOfClass: [GADuplicateView class]])
    {
        NSDictionary *currentPlot = [duplicateList objectAtIndex:indexPath.row];
        
        plotID = [currentPlot objectForKey:@"ID"];
        areaField.text = [currentPlot objectForKey:@"Area"];
        
        if([[currentPlot objectForKey:@"Type"] isEqualToString:@"Residential"])
        {
            [self didTapTypeButton:residentialButton];
        }
        else if ([[currentPlot objectForKey:@"Type"] isEqualToString:@"Commercial"])
        {
            [self didTapTypeButton:commercialButton];
        }
        else
        {
            [commercialButton setTitle:[currentPlot objectForKey:@"Type"] forState:UIControlStateSelected];
            [self didTapTypeButton:commercialButton];
        }
        
        [popupController dismissPopupControllerAnimated:YES];
        
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([tableView.superview isKindOfClass: [GADuplicateView class]])
        return 60;
    
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
     if([tableView.superview isKindOfClass: [GADuplicateView class]])
    {
        return 50;
    }
    
    if(section == 1 && segmentControl.selectedSegmentIndex == 0)
        return 30;
    
    return 0;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
     if([tableView.superview isKindOfClass: [GADuplicateView class]])
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
        label.font = GAMediumFont;
        label.text = [NSString stringWithFormat: @"      Select Plot %@",plotField.text];
        label.backgroundColor = [UIColor whiteColor];
        label.textColor = [UIColor blackColor];
        
        return label;
    }
    
    if(section == 0 || section == 2)
    {
        return nil;
    }
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = GAThemeColor;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-5, 30)];
    titleLabel.font = GASmallFont;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"Price";
    [headerView addSubview:titleLabel];
    
    return headerView;
}

#pragma mark - Keyboard methods

-(void) keyboardWillShow :(NSNotification *) notification{
    
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.tableView setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-kbSize.height-64)];
}

-(void) keyboardWillHide :(NSNotification *) notification{
    
    [self.tableView setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-49)];
}

-(void) priceFieldTextDidChange
{
    float value = [priceField.text floatValue];
    
    float commision = [GAUtilities getPercentSizeWithParentSize:value percent:1];
    
    commisionLabel.text = [NSString stringWithFormat:@"%f",commision];
    totalPriceLabel.text = [NSString stringWithFormat:@"%f",(value+commision)];
    
}

#pragma mark - UITextField Delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
    {
        [textField resignFirstResponder];

        return YES;
    }

@end
