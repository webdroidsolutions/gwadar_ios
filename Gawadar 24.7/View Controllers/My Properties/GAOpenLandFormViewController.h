//
//  GAOpenLandFormViewController.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 17/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GAOpenLandFormViewController : UIViewController

@property (strong , nonatomic) NSString *schemeID;
@property (strong , nonatomic) NSString *subSchemeID;
@property (strong , nonatomic) NSString *mapURLString;

@end
