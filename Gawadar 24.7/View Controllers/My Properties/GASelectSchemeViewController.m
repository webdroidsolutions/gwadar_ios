//
//  GASelectSchemeViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 12/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GASelectSchemeViewController.h"
#import "GAAddPropertyFormViewController.h"
#import "GAOpenLandFormViewController.h"

#import "GASchemesCollectionViewCell.h"


@interface GASelectSchemeViewController () <UICollectionViewDelegate , UICollectionViewDataSource>
{
    NSArray *dataArray;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation GASelectSchemeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Select SubScheme";

    self.automaticallyAdjustsScrollViewInsets = NO;

    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [self callService];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollection View delegate methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return [dataArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    GASchemesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collectionviewcell" forIndexPath:indexPath];
    
    NSDictionary *currentScheme = [dataArray objectAtIndex:indexPath.row];
    
    cell.nameLabel.text = [currentScheme objectForKey:@"Name"];
    [cell.propertyImageView sd_setImageWithURL:[currentScheme objectForKey:@"Image"] placeholderImage:[UIImage imageNamed:@""]];
    
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *currentScheme = [dataArray objectAtIndex:indexPath.row];
    
   if(self.secondForm)
   {
       GAOpenLandFormViewController *openLandVC = [[GAOpenLandFormViewController alloc] init];
       self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
       openLandVC.schemeID = self.ID;
       openLandVC.subSchemeID = [currentScheme objectForKey:@"ID"];
       openLandVC.mapURLString = [currentScheme objectForKey:@"Image"];
       
       
       [self.navigationController pushViewController:openLandVC animated:YES];
       
       return;
   }
    
    GAAddPropertyFormViewController *formViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAAddPropertyFormViewController"];
    formViewController.layerName = [currentScheme objectForKey:@"LayerName"];
    formViewController.ID = [currentScheme objectForKey:@"ID"];
    formViewController.areaUnit = [currentScheme objectForKey:@"AreaUnit"];
    formViewController.SchemeID = self.ID;
    formViewController.imageURLString = [currentScheme objectForKey:@"Image"];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
    
    [self.navigationController pushViewController:formViewController animated:YES];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake((self.view.frame.size.width/2)-5, (self.view.frame.size.width/2)-5);
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GASubSchemeListServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"schemeid" : self.ID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            dataArray = [json objectForKey:@"Subschemes"];
            
            [hud hideAnimated:YES];
            
            [self.collectionView reloadData];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error!" message:@"Error in loading schemes" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error!" message:@"Error in loading schemes" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
        
    }];
    
}

@end
