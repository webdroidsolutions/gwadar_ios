//
//  GASelectSchemeViewController.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 12/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GASelectSchemeViewController : UIViewController

@property (strong , nonatomic) NSString *ID;
@property (nonatomic) BOOL secondForm;

@end
