//
//  GAAddPropertyFormViewController.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 24/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GAAddPropertyFormViewController : UIViewController

@property (strong , nonatomic) NSString *layerName;
@property (strong , nonatomic) NSString *ID;
@property (strong , nonatomic) NSString *areaUnit;
@property (strong , nonatomic) NSString *SchemeID;
@property (strong , nonatomic) NSString *imageURLString;

@end
