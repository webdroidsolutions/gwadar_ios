//
//  GACreateAccountViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 08/02/2018.
//  Copyright © 2018 HomeMade. All rights reserved.
//

#import "GACreateAccountViewController.h"

#import "GAWebServiceSingleton.h"

@interface GACreateAccountViewController () <UITextFieldDelegate>
{
    float scrollViewOriginalHeight;
    float scrollViewY;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *phoneField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UITextField *addressField;
@property (strong, nonatomic) IBOutlet UITextField *organizationField;
@property (strong, nonatomic) IBOutlet UITextField *cnicField;
@property (strong, nonatomic) IBOutlet UIButton *createButton;

@end

@implementation GACreateAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.


    [self configureTextField:self.nameField];
    [self configureTextField:self.phoneField];
    [self configureTextField:self.passwordField];
    [self configureTextField:self.addressField];
    [self configureTextField:self.organizationField];
    [self configureTextField:self.cnicField];

    self.createButton.layer.cornerRadius = 10.0f;

    // keyboard notifications

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

}

-(void)viewDidAppear:(BOOL)animated
{
    scrollViewOriginalHeight = self.scrollView.frame.size.height;
    scrollViewY = self.scrollView.frame.origin.y;
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width
                                             , scrollViewOriginalHeight);
}

#pragma mark - IBActions

- (IBAction)didTapCancelButton:(id)sender {

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapCreateButton:(id)sender {

    if ([self.nameField.text isEqualToString:@""])
    {
        [ISMessages showCardAlertWithTitle:@"Error!" message:@"Enter Name" iconImage:nil duration:2.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

        return;
    }
    if ([self.phoneField.text isEqualToString:@""])
    {
        [ISMessages showCardAlertWithTitle:@"Error!" message:@"Enter Phone Number" iconImage:nil duration:2.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

        return;
    }
    if ([self.passwordField.text isEqualToString:@""])
    {
        [ISMessages showCardAlertWithTitle:@"Error!" message:@"Enter Password" iconImage:nil duration:2.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

        return;
    }

    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];

    NSDictionary *params = @{
                             @"name" : self.nameField.text,
                             @"phone" : self.phoneField.text,
                             @"password" : self.passwordField.text,
                             @"address" : self.addressField.text,
                             @"organization" : self.organizationField.text,
                             @"cnic" : self.cnicField.text
                            };

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [manager POST:GACreateAccountServiceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];

        NSString *errorCode = [json objectForKey:@"ErrorCode"];

        if ([errorCode isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success!" message:@"Account created Successfully" iconImage:nil duration:2.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
                [hud hideAnimated:YES];

            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else
        {
            [ISMessages showCardAlertWithTitle:@"Error!" message:@"Error in creating a new Account" iconImage:nil duration:2.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

            [hud hideAnimated:YES];

        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        [ISMessages showCardAlertWithTitle:@"Error!" message:@"Error in creating a new Account. Try Again Later" iconImage:nil duration:2.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];

        [hud hideAnimated:YES];

    }];

}

#pragma mark - Event Handlers
-(void) keyboardWillShow :(NSNotification *) notification{

    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, scrollViewOriginalHeight);
    if (scrollViewOriginalHeight != self.scrollView.frame.size.height)
        return;

    NSLog(@"Keyboard is shown");
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [self.scrollView setFrame:CGRectMake(0, scrollViewY, self.view.frame.size.width, scrollViewOriginalHeight-kbSize.height)];

}

-(void) keyboardWillHide :(NSNotification *) notification{

    NSLog(@"Keyboard is hidden");
    [self.scrollView setFrame:CGRectMake(0, scrollViewY, self.view.frame.size.width, scrollViewOriginalHeight)];
}

#pragma mark - TextField Methods

-(void) configureTextField : (UITextField *) textField
{
    textField.delegate = self;

    textField.layer.borderWidth = 1.0f;
    textField.layer.borderColor = GAThemeColor.CGColor;
    textField.layer.cornerRadius = 5.0f;

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, textField.frame.size.height)];
    textField.leftView = view;

    textField.leftViewMode = UITextFieldViewModeAlways;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
