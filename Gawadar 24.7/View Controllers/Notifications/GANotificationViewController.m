//
//  GANotificationViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 22/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GANotificationViewController.h"

#import "CNPPopupController.h"
#import "HCSStarRatingView.h"

#import "GAPropertyView.h"

#import "GAConstants.h"

@interface GANotificationViewController () <UITableViewDelegate , UITableViewDataSource>
{
    CNPPopupController *popupController;
    
    NSArray *notificationsArray;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GANotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = GAThemeColor;
    self.navigationItem.title = @"Notifications";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{

    self.tabBarController.viewControllers[4].tabBarItem.badgeValue = nil;

    [self callService];
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GANotificationsListServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            notificationsArray = [json objectForKey:@"Notifications"];
            
            [hud hideAnimated:YES];
            [self.tableView reloadData];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}

-(void) respondToOfferWithNotificationIndex : (long) index response : (NSString *) response
{
    
    NSDictionary *currentNotification = [notificationsArray objectAtIndex:index];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GARespondToNotificationServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"requestid" : [currentNotification objectForKey:@"RequestID"],
                             @"response" : response
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [hud hideAnimated:YES];
            
            [ISMessages showCardAlertWithTitle:@"Success!" message:@"Responded to request successfully" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [hud hideAnimated:YES];

            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"003"])
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You already responded to this request" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Repsodnig to request" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in Responding to request. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
  
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [notificationsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    cell.textLabel.font = GASmallFont;
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.textLabel.text = @"This is a long notification text";
    cell.textLabel.numberOfLines = 0;
    
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 58, self.view.frame.size.width-5, 10)];
    dateLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:8.0f];
    dateLabel.textAlignment = NSTextAlignmentRight;
    dateLabel.textColor = [UIColor lightGrayColor];
    [cell.contentView addSubview:dateLabel];
    
    cell.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row %2 ==0)
        cell.contentView.backgroundColor = GAEvenCellColor;
    else
        cell.contentView.backgroundColor = GAOddCellColor;
    
    // populating data
    NSDictionary *currentNotification = [notificationsArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [currentNotification objectForKey:@"Message"];
    dateLabel.text = [currentNotification objectForKey:@"NotificationTime"];
    
    
    if([[currentNotification objectForKey:@"IsSeen"] isEqualToString:@"true"])
    {
        cell.highlighted = YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *currentPropety = [((NSDictionary *) [notificationsArray objectAtIndex:indexPath.row]) objectForKey:@"Property"];
    
    GAPropertyView *propertyView = [[[NSBundle mainBundle] loadNibNamed:@"PropertyView" owner:self options:nil] objectAtIndex:0];
    
    HCSStarRatingView *ratingView = [[HCSStarRatingView alloc] initWithFrame:propertyView.ratingView.frame];
    ratingView.tintColor = GAThemeColor;
    ratingView.allowsHalfStars = YES;
    ratingView.userInteractionEnabled = NO;
    ratingView.minimumValue = 0;
    ratingView.maximumValue = 5;
    ratingView.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:ratingView];
    
    
    
    propertyView.priceLabel.textColor = GAThemeColor;
    propertyView.imageView.layer.cornerRadius = propertyView.imageView.frame.size.height/2;
    propertyView.imageView.clipsToBounds = YES;
    
    [propertyView.upperButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    [propertyView.lowerButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    
    
    propertyView.upperButton.layer.borderColor = GAThemeColor.CGColor;
    [propertyView.upperButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    propertyView.upperButton.layer.borderWidth = 1.0f;
    propertyView.upperButton.layer.cornerRadius = 5.0f;
    
    propertyView.lowerButton.backgroundColor = GAThemeColor;
    propertyView.lowerButton.layer.cornerRadius = 5.0f;
    [propertyView.lowerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    propertyView.upperButton.tag = indexPath.row;
    propertyView.lowerButton.tag = indexPath.row;
    
    [propertyView.upperButton addTarget:self action:@selector(didTapAcceptOfferButton:) forControlEvents:UIControlEventTouchUpInside];
    [propertyView.lowerButton addTarget:self action:@selector(didTapRejectOfferButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSDictionary *currentNotification = [notificationsArray objectAtIndex:indexPath.row];
    
    if(![[currentNotification objectForKey:@"Type"] isEqualToString:@"0"])
    {
        propertyView.upperButton.hidden = YES;
        propertyView.lowerButton.hidden = YES;
    }
    else
    {
        propertyView.responseImageView.hidden = YES;
        propertyView.lowerLabel.hidden = YES;
        propertyView.upperLabel.hidden = YES;
    }
    
    
    switch ([[currentNotification objectForKey:@"Type"] integerValue]) {
        
        case 0:
            
            break;
            
        case 1:
        {
            propertyView.responseImageView.hidden = YES;
            propertyView.lowerLabel.hidden = YES;
            propertyView.upperLabel.hidden = NO;
            propertyView.upperLabel.text = @"Offer Pending";
            
        }
            break;

            
        case 2:
        {
            propertyView.responseImageView.hidden = YES;
            propertyView.upperLabel.hidden = YES;
            propertyView.lowerLabel.hidden = NO;
            propertyView.lowerLabel.text = @"Offer already accepted";
            
        }            break;


        case 3:
        {
            propertyView.responseImageView.hidden = NO;
            propertyView.upperLabel.hidden = YES;
            propertyView.lowerLabel.hidden = NO;
            propertyView.lowerLabel.text = @"Offer accepted";
            propertyView.responseImageView.image = [UIImage imageNamed:@"accepted.png"];
            
        }
            break;
            
        case 4:
        {
            propertyView.responseImageView.hidden = YES;
            propertyView.lowerLabel.hidden = YES;
            propertyView.upperLabel.hidden = NO;
            propertyView.upperLabel.text = @"User accepted this offer";
        }
            break;
            
        case 5:
        {
            propertyView.responseImageView.hidden = YES;
            propertyView.upperLabel.hidden = YES;
            propertyView.lowerLabel.hidden = NO;
            propertyView.lowerLabel.text = @"Offer already rejected";
            
        }
            break;
            
        case 6:
        {
            propertyView.responseImageView.hidden = NO;
            propertyView.upperLabel.hidden = YES;
            propertyView.lowerLabel.hidden = NO;
            propertyView.lowerLabel.text = @"Offer Rejected";
            propertyView.responseImageView.image = [UIImage imageNamed:@"rejected.png"];
            
            
        }
            break;
            
        case 7:
        {
            
            propertyView.responseImageView.hidden = YES;
            propertyView.upperLabel.hidden = YES;
            propertyView.lowerLabel.hidden = NO;
            propertyView.lowerLabel.text = @"User rejected this offer";
            
        }
            break;
            
        default:
            propertyView.lowerButton.hidden = YES;
            propertyView.upperButton.hidden = YES;
            break;
    }
    
    propertyView.priceLabel.text = [currentPropety objectForKey:@"Price"];
    propertyView.nameLabel.text = [currentPropety objectForKey:@"Line1Text"];
    propertyView.label2.text = [currentPropety objectForKey:@"Line2Text"];
    propertyView.label3.text = [currentPropety objectForKey:@"Line3Text"];
    propertyView.label4.text = [currentPropety objectForKey:@"Line4Text"];
    propertyView.label5.text = [currentPropety objectForKey:@"Line5Text"];
    propertyView.label6.text = [currentPropety objectForKey:@"Line6Text"];
    
    
    propertyView.icon1.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@.png",[(NSString *)[currentPropety objectForKey:@"Line1Icon"] lowercaseString]]
                        ];
    propertyView.icon2.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentPropety objectForKey:@"Line2Icon"] lowercaseString]]
                        ];
    propertyView.icon3.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentPropety objectForKey:@"Line3Icon"] lowercaseString]]
                        ];
    propertyView.icon4.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentPropety objectForKey:@"Line4Icon"] lowercaseString]]
                        ];
    propertyView.icon5.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentPropety objectForKey:@"Line5Icon"] lowercaseString]]
                        ];
    propertyView.icon6.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentPropety objectForKey:@"Line6Icon"] lowercaseString]]
                        ];
    
    
    ratingView.value = [[currentPropety objectForKey:@"Rating"] floatValue];
    
    
    popupController = [[CNPPopupController alloc] initWithContents:@[propertyView]];
    
    popupController.theme = [CNPPopupTheme defaultTheme];
    
    CNPPopupTheme *theme = [[CNPPopupTheme alloc] init];
    
    theme.presentationStyle = CNPPopupPresentationStyleFadeIn;
    theme.shouldDismissOnBackgroundTouch = YES;
    theme.backgroundColor = [UIColor whiteColor];
    theme.popupStyle = CNPPopupStyleCentered;
    theme.maskType = CNPPopupMaskTypeDimmed;
    theme.maxPopupWidth = self.view.frame.size.width-10;
    theme.dismissesOppositeDirection = YES;
    theme.animationDuration = 0.5f;
    theme.contentVerticalPadding = 5.0f;
    theme.cornerRadius = 10.0f;
    theme.movesAboveKeyboard = NO;
    
    
    popupController.theme = theme;

    
    [popupController presentPopupControllerAnimated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

#pragma mark - event handlers

-(void) didTapAcceptOfferButton : (id) sender
{
    [popupController dismissPopupControllerAnimated:YES];
    long index = ((UIButton *) sender).tag;
    [self respondToOfferWithNotificationIndex:index response:@"true"];
    
}

-(void) didTapRejectOfferButton : (id) sender
{
    [popupController dismissPopupControllerAnimated:YES];
    long index = ((UIButton *) sender).tag;
    [self respondToOfferWithNotificationIndex:index response:@"false"];
}

@end
