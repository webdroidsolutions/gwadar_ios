//
//  GAMyNeedsViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/10/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAMyNeedsViewController.h"

#import "GAHomeTableViewCell.h"

#import "HCSStarRatingView.h"


@interface GAMyNeedsViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *wantedArray;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GAMyNeedsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"My Needs";
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:233/255.0f green:235/255.0f blue:238/255.0f alpha:1.0];
    
    self.tableView.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-49);
    
    [self callService];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GAMyPropertiesServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            NSDictionary *properties = [json objectForKey:@"Properties"];
            
            wantedArray = ((NSArray *)[properties objectForKey:@"Wanted"]).mutableCopy;
            
            [hud hideAnimated:YES];
            
            [self.tableView reloadData];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [hud hideAnimated:YES];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
    
    
}

-(void) addToFavouriteWithPropertyId : (NSString *) propertyID
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GAAddToFavouriteServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property has been added to favourite list" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in adding this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in adding this property to list. Try again later." iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}

-(void) cancelRequestToProprtyID : (NSString *) propertyID targetButton : (UIButton *) button
{
    long index = button.tag;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GACancelInquiryServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        NSLog(@"the dictionary : %@",json);
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property has been deleted successfully" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [wantedArray removeObjectAtIndex:index];
            
            [self.tableView beginUpdates];
            
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
            
            [self.tableView endUpdates];
            
            
            [UIView animateWithDuration:0.1 delay:2 options:UIViewAnimationOptionTransitionNone animations:^{
                [self.tableView reloadData];
            } completion:nil];
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"001"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Property Already Sold!" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"002"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You cannot send inquiry to your own property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"003"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You already have a pending request for this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in canceling the inquiry" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in canceling the inquiry. try again leter" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}



#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return wantedArray.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GAHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GAHomeTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row%2 == 0)
        cell.contentView.backgroundColor = GAEvenCellColor;
    else
        cell.contentView.backgroundColor = GAOddCellColor;
    HCSStarRatingView *ratingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-100, cell.priceLabel.frame.origin.y-5,80, 40)];
    ratingView.tintColor = GAThemeColor;
    ratingView.minimumValue = 0;
    ratingView.maximumValue = 5;
    ratingView.userInteractionEnabled = NO;
    ratingView.backgroundColor = [UIColor clearColor];
    ratingView.allowsHalfStars = YES;
    
    [cell.contentView addSubview:ratingView];
    
    
    cell.priceLabel.textColor = GAThemeColor;
    cell.propertyImageView.layer.cornerRadius = cell.propertyImageView.frame.size.height/2;
    cell.propertyImageView.clipsToBounds = YES;
    
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    [cell.sendInquiryButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    
    
    cell.addToFavrtButton.layer.borderColor = GAThemeColor.CGColor;
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    cell.addToFavrtButton.layer.borderWidth = 1.0f;
    cell.addToFavrtButton.layer.cornerRadius = 5.0f;
    
    cell.sendInquiryButton.backgroundColor = GAThemeColor;
    cell.sendInquiryButton.layer.cornerRadius = 5.0f;
    [cell.sendInquiryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    // data populating
    NSDictionary *currentProperty = [wantedArray objectAtIndex:indexPath.row];
    
    ratingView.value = [[currentProperty objectForKey:@"Rating"] floatValue];
    cell.priceLabel.text = [currentProperty objectForKey:@"Price"];
    
    cell.areaLabel.text = [currentProperty objectForKey:@"SubSchemeName"];
    
    
    cell.sizeLabel.text = [currentProperty objectForKey:@"Layer2Name"];
    cell.phaseLabel.text = [currentProperty objectForKey:@"PlotType"];
    cell.schemeLabel.text = [currentProperty objectForKey:@"Layer1Name"];
    cell.subschemeLabel.text = [currentProperty objectForKey:@"PlotNo"];
    cell.lastLabel.text = [currentProperty objectForKey:@"Area"];
    
    cell.typeImageView.image = [UIImage imageNamed:
                                [NSString stringWithFormat:@"%@.png",[(NSString *)[currentProperty objectForKey:@"PlotType"] lowercaseString]]
                                ];
    
    
    [cell.propertyImageView sd_setImageWithURL:[currentProperty objectForKey:@"Image"] placeholderImage:[UIImage imageNamed:@""]];
    
    
        cell.sendInquiryButton.tag = indexPath.row;
        cell.addToFavrtButton.tag = indexPath.row ;
    
    
        [cell.sendInquiryButton setTitle:@"Delete Property" forState:UIControlStateNormal];
        [cell.sendInquiryButton addTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    // new data
    cell.line1.text = [currentProperty objectForKey:@"Line1Text"];
    cell.line2.text = [currentProperty objectForKey:@"Line2Text"];
    cell.line3.text = [currentProperty objectForKey:@"Line3Text"];
    cell.line4.text = [currentProperty objectForKey:@"Line4Text"];
    cell.line5.text = [currentProperty objectForKey:@"Line5Text"];
    cell.line6.text = [currentProperty objectForKey:@"Line6Text"];
    
    
    cell.icon1.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@.png",[(NSString *)[currentProperty objectForKey:@"Line1Icon"] lowercaseString]]
                        ];
    cell.icon2.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line2Icon"] lowercaseString]]
                        ];
    cell.icon3.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line3Icon"] lowercaseString]]
                        ];
    cell.icon4.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line4Icon"] lowercaseString]]
                        ];
    cell.icon5.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line5Icon"] lowercaseString]]
                        ];
    cell.icon6.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line6Icon"] lowercaseString]]
                        ];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 171;
}

#pragma mark - event handlers


-(void) didTapAddFavouriteButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    
        currentProperty = [wantedArray objectAtIndex:tag];
    
    [self addToFavouriteWithPropertyId:[currentProperty objectForKey:@"ID"]];
}

-(void) didTapCancelRequestButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
        currentProperty = [wantedArray objectAtIndex:tag];
    
    [self cancelRequestToProprtyID:[currentProperty objectForKey:@"ID"] targetButton:(UIButton *) sender];
    
}

@end
