//
//  GAImageViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 12/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAImageViewController.h"

@interface GAImageViewController () <UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation GAImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.imageView sd_setImageWithURL:self.imageURL];
    self.scrollview.delegate = self;
    
    self.navigationItem.title = self.title;
}

#pragma mark - uiscrollview delegate methods

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

@end
