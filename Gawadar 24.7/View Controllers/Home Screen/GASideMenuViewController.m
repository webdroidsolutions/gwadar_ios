//
//  GASideMenuViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 12/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GASideMenuViewController.h"
#import "GAHomeScreenViewController.h"
#import "GANotificationViewController.h"
#import "GAAddPropertyViewController.h"
#import "GAMyPropertiesViewController.h"
#import "GAMapsViewController.h"
#import "GANewsFlashViewController.h"
#import "GAGeneralPropertyViewController.h"
#import "GAMyNeedsViewController.h"
#import "GAGoogleMapsViewController.h"
#import "GAMyRequestViewController.h"

@interface GASideMenuViewController ()<UITableViewDelegate , UITableViewDataSource>
{
    NSArray *titlesArray;
    NSArray *imagesArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GASideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    
    titlesArray = @[
                    @[@"News Feed",@"Favourites",@"Notifications"],
                    @[@"Add Property",@"My Properties",@"My Needs",@"My Requests"],
                    @[@"Master Plan",@"Gwadar Map",@"District Map"],
                    @[@"News Flash"],
                    @[@"Terms & Conditions",@"About Us",@"Contact Us",@"Log Out"]
                    ];
    
    imagesArray = @[
                    @[@"home.png",@"fvrt.png",@"globe.png"],
            @[@"add.png",@"myproperties.png",@"needs.png",@"needs.png"],
                    @[@"master.png",@"gms.png",@"district.png"],
                    @[@"map.png"],
                  @[@"about.png",@"about.png",@"contact.png",@"logout.png"]
                    ];
    
    
    self.tableView.frame = CGRectMake(0, 64, [GAUtilities getPercentSizeWithParentSize:self.view.frame.size.width percent:80], self.view.frame.size.height-64-49);
    
    self.tableView.tableHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"SideMenuHeaderView" owner:self options:nil] objectAtIndex:0];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - web service methods

-(void) contact
{
    NSString *serviceURL = GAContactAdminServiceURL;
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager POST:serviceURL parameters:@{} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([json objectForKey:@"Phone"])
        {
            NSString *phone = [json objectForKey:@"Phone"];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phone]]];
        }
        else
        {
            [ISMessages showCardAlertWithTitle:@"ErrorQ" message:@"Error in contacting admin. Try Again Later" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [ISMessages showCardAlertWithTitle:@"ErrorQ" message:@"Error in contacting admin. Try Again Later" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
        
    }];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [titlesArray objectAtIndex:section];
    
    return [array count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array = [titlesArray objectAtIndex:indexPath.section];
    NSArray *images = [imagesArray objectAtIndex:indexPath.section];
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.font = GASmallFont;
    cell.textLabel.text = [array objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    cell.imageView.contentMode = UIViewContentModeCenter;
    cell.imageView.image = [UIImage imageNamed:[images objectAtIndex:indexPath.row]];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            
            switch (indexPath.row) {
                case 0:
                {
                    GAHomeScreenViewController *homeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAHomeScreenViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    [self.navigationController pushViewController:homeVC animated:YES];
                    
                }
                    break;
                    
                case 1:
                {
                    
                    GAGeneralPropertyViewController *generalVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAGeneralPropertyViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    generalVC.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];

                    [self.navigationController pushViewController:generalVC animated:YES];
                }
                    break;
                    
                case 2:
                {
                    
                    GANotificationViewController *homeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GANotificationViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    [self.navigationController pushViewController:homeVC animated:YES];
                }
                    break;
                    
                default:
                    break;
            }
            
            break;
           
        case 1:
            switch (indexPath.row) {
                case 0:
                {
                    GAAddPropertyViewController *homeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAAddPropertyViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    [self.navigationController pushViewController:homeVC animated:YES];
                }
                    break;
                    
                case 1:
                {
                    GAMyPropertiesViewController *homeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAMyPropertiesViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    [self.navigationController pushViewController:homeVC animated:YES];
                }
                    break;
                    
                case 2:
                {
                    GAMyNeedsViewController *myNeedsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAMyNeedsViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    [self.navigationController pushViewController:myNeedsVC animated:YES];
                }
                    break;
                    
                case 3:
                {
                    GAMyRequestViewController *MYRequestVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAMyRequestViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                    [self.navigationController pushViewController:MYRequestVC animated:YES];
                }
                    break;
                    
                default:
                    break;
            }
            break;
            
        case 2:
            switch (indexPath.row) {
                    
                case 1:
                {
                    GAGoogleMapsViewController *googleMapVC = [[GAGoogleMapsViewController alloc] init];
                    
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self  action:nil];
                    
                    [self.navigationController pushViewController:googleMapVC animated:YES];
                    
                }
                    break;
                
                case 0:
                case 2:
                {
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    
                    NSString *serviceURL = indexPath.row == 0? GAMasterPlanServiceURL:GADistrictMapServiceURL;
                    
                    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
                    
                    [manager POST:serviceURL parameters:@{} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        
                        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
                        
                        if([json objectForKey:@"Image"])
                        {
                            NSString *imageLink = [json objectForKey:@"Image"];
                            
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",imageLink]]];
                            
                            [hud hideAnimated:YES];
                        }
                        else
                        {
                            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Loading Image. Try Again Later" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
                            
                                                        [hud hideAnimated:YES];
                        }
                        
                    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        
                        [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in Loading Image. Try Again Later" iconImage:nil duration:5.0 hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
                    
                        [hud hideAnimated:YES];
                        
                    }];
                    
                }
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        case 3:
            switch (indexPath.row) {
                case 0:
                {
                    GANewsFlashViewController *homeVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GANewsFlashViewController"];
                    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
                     [self.navigationController pushViewController:homeVC animated:YES];
                }
                    break;
                    
                default:
                    break;
            }
            break;
            
        case 4:
            switch (indexPath.row) {
                case 0:
                    //terms ans condition
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://gwadar247.pk/terms.html"]];
                    break;
                    
                case 1:
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://gwadar247.pk/about.html"]];
                    break;
                    
                case 2:
                {
                    [self contact];
                }
                    break;
                    
                case 3:
                {
                    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:GALoginInformationDictionaryKey];
                    
                    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
                    GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
                    
                    window.rootViewController = loginVC;
                }
                    break;
                    
                    
                default:
                    break;
            }
            break;
            
        default:
            break;
    }
}

@end
