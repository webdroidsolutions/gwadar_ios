//
//  GAMapsListingsViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 10/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAMapsListingsViewController.h"
#import "GAImageViewController.h"

@interface GAMapsListingsViewController () <UITableViewDelegate , UITableViewDataSource>
{
    NSArray *dataArray;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GAMapsListingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.barTintColor = GAThemeColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.navigationItem.title = @"Maps";

    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self callService];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GAMapsListServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            dataArray = [json objectForKey:@"Maps"];
            
            [hud hideAnimated:YES];
            
            [self.tableView reloadData];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            if(![[json objectForKey:@"UnseenNotifications"] isEqualToString:@"0"] || ![json objectForKey:@"UnseenNotifications"])
            {
                self.tabBarController.viewControllers[4].tabBarItem.badgeValue = [json objectForKey:@"UnseenNotifications"];
            }        else
                self.tabBarController.viewControllers[4].tabBarItem.badgeValue = nil;


            [hud hideAnimated:YES];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading Maps" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading Maps. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
    
    
}


#pragma mark - UITableView methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    NSDictionary *currentMap = [dataArray objectAtIndex:indexPath.row];
   
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.view.frame.size.width-60, 50)];
    nameLabel.text = [currentMap objectForKey:@"Name"];
    nameLabel.font = GASmallFont;
    nameLabel.textColor = [UIColor darkGrayColor];
    nameLabel.numberOfLines = 0;
    [cell.contentView addSubview:nameLabel];
    
    
    UIImageView *mapImage = [[UIImageView alloc] init];
    mapImage.frame = CGRectMake(0, 0, 20, 20);
    [mapImage sd_setImageWithURL:[NSURL URLWithString:[currentMap objectForKey:@"Thumb"]]];

    cell.accessoryView = mapImage;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *currentMap = [dataArray objectAtIndex:indexPath.row];
    
//    GAImageViewController *imageVc = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GAImageViewController"];
//    imageVc.imageURL = [NSURL URLWithString:[currentMap objectForKey:@"Thumb"]];
//
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
//    [self.navigationController pushViewController:imageVc animated:YES];
    
    NSURL *imageURL = [NSURL URLWithString:[currentMap objectForKey:@"Image"]];
    [[UIApplication sharedApplication] openURL:imageURL];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

@end
