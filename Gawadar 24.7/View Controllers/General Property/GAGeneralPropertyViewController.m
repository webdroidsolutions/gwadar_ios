//
//  GAGeneralPropertyViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 17/09/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAGeneralPropertyViewController.h"

#import "GAHomeTableViewCell.h"

#import "GAPropertyView.h"
#import "HCSStarRatingView.h"

@interface GAGeneralPropertyViewController () <UITableViewDelegate , UITableViewDataSource>
{
    UISegmentedControl *headerSegmentControl;

    NSMutableArray *saleArray;
    NSMutableArray *wantedArray;
    
    NSMutableArray *favouriteArray;
}
@property (strong , nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GAGeneralPropertyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.barTintColor = GAThemeColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.navigationItem.title = @"Favourites";
    
    // header segment control
    headerSegmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Sale",@"Wanted"]];
    headerSegmentControl.tintColor = GAThemeColor;
    headerSegmentControl.selectedSegmentIndex = 0;
    headerSegmentControl.backgroundColor = [UIColor whiteColor];
    [headerSegmentControl addTarget:self action:@selector(didChangeSegmentControlValue) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self callService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GAFavouritePropertiesServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {


            if(![[json objectForKey:@"UnseenNotifications"] isEqualToString:@"0"] || ![json objectForKey:@"UnseenNotifications"])
            {
                self.tabBarController.viewControllers[4].tabBarItem.badgeValue = [json objectForKey:@"UnseenNotifications"];
            }        else
                self.tabBarController.viewControllers[4].tabBarItem.badgeValue = nil;

            NSDictionary *properties = [json objectForKey:@"Properties"];
            
            saleArray = ((NSArray *)[properties objectForKey:@"Sale"]).mutableCopy;
            wantedArray = ((NSArray *)[properties objectForKey:@"Wanted"]).mutableCopy;
            
            [hud hideAnimated:YES];
            
            [self.tableView reloadData];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
    
    
}

-(void) sendInquiryToPropertyId : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GASendInquiryServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Inquiry has been sended successfully to property owner" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Delete Inquiry" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
            
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in sending the inquiry" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in sending the inquiry. try again leter" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}



-(void) cancelRequestToProprtyID : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GACancelInquiryServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        NSLog(@"the dictionary : %@",json);
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Inquiry has been canceled successfull" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Send Inquiry" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapSendIquiryButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"001"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Property Already Sold!" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"002"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You cannot send inquiry to your own property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"003"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You already have a pending request for this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in canceling the inquiry" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in canceling the inquiry. try again leter" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long count = (headerSegmentControl.selectedSegmentIndex == 0)? [saleArray count] : [wantedArray count];
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GAHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GAHomeTableViewCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if(indexPath.row%2 == 0)
        cell.contentView.backgroundColor = GAEvenCellColor;
    else
        cell.contentView.backgroundColor = GAOddCellColor;
    HCSStarRatingView *ratingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-100, cell.priceLabel.frame.origin.y-5,80, 40)];
    ratingView.tintColor = GAThemeColor;
    ratingView.minimumValue = 0;
    ratingView.maximumValue = 5;
    ratingView.backgroundColor = [UIColor clearColor];
    ratingView.allowsHalfStars = YES;
    ratingView.userInteractionEnabled = NO;
    [cell.contentView addSubview:ratingView];
    
    
    cell.priceLabel.textColor = GAThemeColor;
    cell.propertyImageView.layer.cornerRadius = cell.propertyImageView.frame.size.height/2;
    cell.propertyImageView.clipsToBounds = YES;
    
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    [cell.sendInquiryButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    
    
    cell.addToFavrtButton.layer.borderColor = GAThemeColor.CGColor;
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    cell.addToFavrtButton.layer.borderWidth = 1.0f;
    cell.addToFavrtButton.layer.cornerRadius = 5.0f;
    
    cell.sendInquiryButton.backgroundColor = GAThemeColor;
    cell.sendInquiryButton.layer.cornerRadius = 5.0f;
    [cell.sendInquiryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [cell.addToFavrtButton setTitle:@"Remove Property" forState:UIControlStateNormal];
    [cell.sendInquiryButton setTitle:@"Send Inquiry" forState:UIControlStateNormal];
    
    // data populating
    NSArray *array = (headerSegmentControl.selectedSegmentIndex == 0)? saleArray : wantedArray;
    
    NSDictionary *currentProperty = [array objectAtIndex:indexPath.row];
    
    ratingView.value = [[currentProperty objectForKey:@"Rating"] floatValue];
    cell.priceLabel.text = [currentProperty objectForKey:@"Price"];
    cell.areaLabel.text = [currentProperty objectForKey:@"Area"];
    cell.sizeLabel.text = [currentProperty objectForKey:@"Area"];
    cell.phaseLabel.text = [currentProperty objectForKey:@"PlotType"];
    cell.schemeLabel.text = [currentProperty objectForKey:@"SchemeName"];
    cell.subschemeLabel.text = [currentProperty objectForKey:@"SubSchemeName"];
    cell.phaseLabel.text = [currentProperty objectForKey:@"Layer1Name"];
    [cell.propertyImageView sd_setImageWithURL:[currentProperty objectForKey:@"Image"] placeholderImage:[UIImage imageNamed:@""]];
    
    
    cell.sendInquiryButton.tag = indexPath.row;
    cell.addToFavrtButton.tag = indexPath.row ;
    
    [cell.addToFavrtButton addTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    // new data
    cell.line1.text = [currentProperty objectForKey:@"Line1Text"];
    cell.line2.text = [currentProperty objectForKey:@"Line2Text"];
    cell.line3.text = [currentProperty objectForKey:@"Line3Text"];
    cell.line4.text = [currentProperty objectForKey:@"Line4Text"];
    cell.line5.text = [currentProperty objectForKey:@"Line5Text"];
    cell.line6.text = [currentProperty objectForKey:@"Line6Text"];
    
    
    cell.icon1.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@.png",[(NSString *)[currentProperty objectForKey:@"Line1Icon"] lowercaseString]]
                        ];
    cell.icon2.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line2Icon"] lowercaseString]]
                        ];
    cell.icon3.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line3Icon"] lowercaseString]]
                        ];
    cell.icon4.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line4Icon"] lowercaseString]]
                        ];
    cell.icon5.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line5Icon"] lowercaseString]]
                        ];
    cell.icon6.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line6Icon"] lowercaseString]]
                        ];
    
    if([[currentProperty objectForKey:@"IsRequested"] isEqualToString:@"true"])
    {
        [cell.sendInquiryButton setTitle:@"Delete Inquiry" forState:UIControlStateNormal];
        [cell.sendInquiryButton addTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.sendInquiryButton setTitle:@"Send Inquiry" forState:UIControlStateNormal];
        
        [cell.sendInquiryButton addTarget:self action:@selector(didTapSendIquiryButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 171;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return headerSegmentControl;
}


#pragma mark - event handlers

-(void) didTapSendIquiryButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSArray *array = (headerSegmentControl.selectedSegmentIndex == 0)? saleArray : wantedArray;
    
    NSDictionary *currentProperty = [array objectAtIndex:tag];
    
    [self sendInquiryToPropertyId:[currentProperty objectForKey:@"ID"] targetButton:((UIButton *) sender)];
    
}

-(void) didTapCancelRequestButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self cancelRequestToProprtyID:[currentProperty objectForKey:@"ID"] targetButton:(UIButton *) sender];
    
}

-(void) didTapAddFavouriteButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSMutableArray *array = (headerSegmentControl.selectedSegmentIndex == 0)? saleArray : wantedArray;
    
    NSDictionary *currentProperty = [array objectAtIndex:tag];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GADeleteFromFavouriteServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : [currentProperty objectForKey:@"ID"]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [hud hideAnimated:YES];

            [ISMessages showCardAlertWithTitle:@"Success!" message:@"Property deleted from favourites successfully" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [array removeObjectAtIndex:tag];
            
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:tag inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
            [self.tableView endUpdates];
            
            [UIView animateWithDuration:0.1 delay:2 options:UIViewAnimationOptionTransitionNone animations:^{
                
            } completion:^(BOOL finished) {
                [self.tableView reloadData];
            }];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [hud hideAnimated:YES];

            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

-(void) didChangeSegmentControlValue
{
    [self.tableView reloadData];
}

@end
