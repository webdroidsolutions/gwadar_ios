//
//  GAGoogleMapsViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 26/10/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GAGoogleMapsViewController.h"
#import "GASearchByPinViewController.h"

#import <GoogleMaps/GoogleMaps.h>


@interface GAGoogleMapsViewController () <GMSMapViewDelegate>
{
    GMSMapView *mapView;
    
    NSArray *pinsArray;
}
@end

@implementation GAGoogleMapsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:25.1987 longitude:62.3213 zoom:12];
    
    mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    self.view = mapView;

    self.navigationItem.title = @"Gwadar Map";
    
    UIBarButtonItem *returnItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"root.png"] style:UIBarButtonItemStyleDone target:self action:@selector(didTapReturnItem)];
    self.navigationItem.rightBarButtonItem = returnItem;
    
    [self callService];
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GAMapPinsServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            pinsArray = [json objectForKey:@"Pins"];
            
            
            for (NSDictionary *dict in pinsArray)
            {
                GMSMarker *marker = [[GMSMarker alloc] init];
                
                CLLocationCoordinate2D cordinates;
                cordinates.longitude = (CLLocationDegrees) [[dict objectForKey:@"Longitude"] doubleValue];
                cordinates.latitude = (CLLocationDegrees) [[dict objectForKey:@"Latitude"] doubleValue];
                
                marker.position = cordinates;
                marker.icon = [GMSMarker markerImageWithColor:GAThemeColor];
                marker.userData = dict;
                marker.map = mapView;
                
            }
            
            [hud hideAnimated:YES];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [hud hideAnimated:YES];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
    
    
}


#pragma mark - GMSMapView delegate methods

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    NSDictionary *data = marker.userData;
    
    GASearchByPinViewController *searchVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GASearchByPinViewController"];
    searchVC.pinID = [data objectForKey:@"ID"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
    
    [self.navigationController pushViewController:searchVC animated:YES];
    
    
    
    
    return YES;
}


#pragma mark - event handlers

-(void) didTapReturnItem
{
    [mapView animateToLocation:CLLocationCoordinate2DMake(25.1987, 62.3213)];
    [mapView animateToZoom:12.0f];
}

@end
