//
//  GASearchByPinViewController.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 27/10/2017.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GASearchByPinViewController : UIViewController

@property (strong , nonatomic) NSString *pinID;

@end
