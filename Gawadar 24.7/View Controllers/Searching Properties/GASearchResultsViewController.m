//
//  GASearchResultsViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GASearchResultsViewController.h"

#import "CNPPopupController.h"
#import "HCSStarRatingView.h"

#import "GAHomeTableViewCell.h"

#import "GAFilterView.h"

#import "ActionSheetStringPicker.h"

@interface GASearchResultsViewController () <UITableViewDelegate , UITableViewDataSource>
{
    UISegmentedControl *headerSegmentControl;
    
    CNPPopupController *popupController;
    
    GAFilterView *filterView;
    
    UITableView *popupTableView;
    
    NSArray *saleArray;
    NSArray *wantedArray;
    
    NSMutableArray *layer1Array;
    NSMutableArray *layer2Array;
    
    NSString *layer1String;
    NSString *layer2String;
    NSString *priceFromString;
    NSString *priceToString;
    NSString *areaFromString;
    NSString *areaToString;
    NSString *sortString;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GASearchResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationItem.title = @"Search Results";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    // header segment control
    headerSegmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Sale",@"Wanted"]];
    headerSegmentControl.tintColor = GAThemeColor;
    headerSegmentControl.selectedSegmentIndex = 0;
    headerSegmentControl.backgroundColor = [UIColor whiteColor];
    [headerSegmentControl addTarget:self action:@selector(didChangeSegmentControlValue) forControlEvents:UIControlEventValueChanged];
    
    
    
    UIBarButtonItem *filterItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter.png"] style:UIBarButtonItemStyleDone target:self action:@selector(didTapFiterItem)];
    self.navigationItem.rightBarButtonItem = filterItem;
    
    [self callService];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64-49)];
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults]
                              objectForKey:GALoginInformationDictionaryKey];
    
    NSString *serviceURL = [NSString stringWithFormat:@"%@?userid=%@&&token=%@"
                            ,GASearchResultServiceURL,[userInfo objectForKey:GALoginInfoLoginIdKey],[userInfo objectForKey:GALoginInfoLoginTokenKey]];
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    NSLog(@"searching from : %@",self.params);
    
    [manager POST:serviceURL parameters:self.params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            NSDictionary *properties = [json objectForKey:@"Properties"];
            
            layer1Array = [[NSMutableArray alloc] init];
            layer2Array = [[NSMutableArray alloc] init];
            
            saleArray = [properties objectForKey:@"Sale"];
            wantedArray = [properties objectForKey:@"Wanted"];
            NSArray * tempArray = [((NSDictionary *)[json objectForKey:@"Filters"]) objectForKey:@"Layer1"];
            
            for (NSDictionary *dict in tempArray)
            {
                [layer1Array addObject:[dict objectForKey:@"Name"]];
            }
            
            tempArray = [((NSDictionary *)[json objectForKey:@"Filters"]) objectForKey:@"Layer2"];
            
            for (NSDictionary *dict in tempArray)
            {
                [layer2Array addObject:[dict objectForKey:@"Name"]];
            }
            
            [hud hideAnimated:YES];
            
            [self.tableView reloadData];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

-(void) addToFavouriteWithPropertyId : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GAAddToFavouriteServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property has been added to favourite list" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Delete Favorite" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapDeleteFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in adding this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in adding this property to list. Try again later." iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}

-(void) deleteFromFavouriteWithPropertyId : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GADeleteFromFavouriteServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property has been deleted from favourite list" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Add to Favorite" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapDeleteFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in adding this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in adding this property to list. Try again later." iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}


-(void) sendInquiryToPropertyId : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GASendInquiryServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        NSLog(@"the dictionary : %@",json);
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Inquiry has been sended successfully to property owner" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Delete Inquiry" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapSendIquiryButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"001"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Property Already Sold!" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"002"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You cannot send inquiry to your own property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"003"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You already have a pending request for this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in sending the inquiry" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in sending the inquiry. try again leter" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}


-(void) cancelRequestToProprtyID : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GACancelInquiryServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        NSLog(@"the dictionary : %@",json);
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Inquiry has been canceled successfull" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Send Inquiry" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapSendIquiryButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"001"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Property Already Sold!" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"002"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You cannot send inquiry to your own property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"003"]){
            
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"You already have a pending request for this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in canceling the inquiry" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in canceling the inquiry. try again leter" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (headerSegmentControl.selectedSegmentIndex == 0)? saleArray.count : wantedArray.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GAHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GAHomeTableViewCell"];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row%2 == 0)
    cell.contentView.backgroundColor = GAEvenCellColor;
    else
    cell.contentView.backgroundColor = GAOddCellColor;
    
    HCSStarRatingView *ratingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-100, cell.priceLabel.frame.origin.y-5,80, 40)];
    ratingView.tintColor = GAThemeColor;
    ratingView.minimumValue = 0;
    ratingView.maximumValue = 5;
    ratingView.backgroundColor = [UIColor clearColor];
    ratingView.userInteractionEnabled = NO;
    [cell.contentView addSubview:ratingView];
    
    
    cell.priceLabel.textColor = GAThemeColor;
    cell.propertyImageView.layer.cornerRadius = cell.propertyImageView.frame.size.height/2;
    cell.propertyImageView.clipsToBounds = YES;
    
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    [cell.sendInquiryButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    
    
    cell.addToFavrtButton.layer.borderColor = GAThemeColor.CGColor;
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    cell.addToFavrtButton.layer.borderWidth = 1.0f;
    cell.addToFavrtButton.layer.cornerRadius = 5.0f;
    
    cell.sendInquiryButton.backgroundColor = GAThemeColor;
    cell.sendInquiryButton.layer.cornerRadius = 5.0f;
    [cell.sendInquiryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    // data populating
    NSArray *array = (headerSegmentControl.selectedSegmentIndex == 0)? saleArray : wantedArray;
    
    NSDictionary *currentProperty = [array objectAtIndex:indexPath.row];
    
    ratingView.value = [[currentProperty objectForKey:@"Rating"] floatValue];
    cell.line1.text = [currentProperty objectForKey:@"Line1Text"];
    cell.line2.text = [currentProperty objectForKey:@"Line2Text"];
    cell.line3.text = [currentProperty objectForKey:@"Line3Text"];
    cell.line4.text = [currentProperty objectForKey:@"Line4Text"];
    cell.line5.text = [currentProperty objectForKey:@"Line5Text"];
    cell.line6.text = [currentProperty objectForKey:@"Line6Text"];
   
    cell.priceLabel.text = [currentProperty objectForKey:@"Price"];
    cell.icon1.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@.png",[(NSString *)[currentProperty objectForKey:@"Line1Icon"] lowercaseString]]
                        ];
    cell.icon2.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line2Icon"] lowercaseString]]
                        ];
    cell.icon3.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line3Icon"] lowercaseString]]
                        ];
    cell.icon4.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line4Icon"] lowercaseString]]
                        ];
    cell.icon5.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line5Icon"] lowercaseString]]
                        ];
    cell.icon6.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line6Icon"] lowercaseString]]
                        ];
    
    
    [cell.propertyImageView sd_setImageWithURL:[currentProperty objectForKey:@"Image"] placeholderImage:[UIImage imageNamed:@""]];
    
    
    if(headerSegmentControl.selectedSegmentIndex == 0)
    {
        cell.sendInquiryButton.tag = indexPath.row;
        cell.addToFavrtButton.tag = indexPath.row ;
    }
    else if (headerSegmentControl.selectedSegmentIndex == 1)
    {
        cell.sendInquiryButton.tag = indexPath.row * -1;
        cell.addToFavrtButton.tag = indexPath.row * -1 ;
    }
    
    if([[currentProperty objectForKey:@"IsRequested"] isEqualToString:@"true"])
    {
        [cell.sendInquiryButton setTitle:@"Delete Inquiry" forState:UIControlStateNormal];
        [cell.sendInquiryButton addTarget:self action:@selector(didTapCancelRequestButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.sendInquiryButton setTitle:@"Send Inquiry" forState:UIControlStateNormal];
        
        [cell.sendInquiryButton addTarget:self action:@selector(didTapSendIquiryButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    if([[currentProperty objectForKey:@"IsFavorite"] isEqualToString:@"true"])
    {
        [cell.addToFavrtButton setTitle:@"Delete Favorite" forState:UIControlStateNormal];
        [cell.addToFavrtButton addTarget:self action:@selector(didTapDeleteFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.addToFavrtButton setTitle:@"Add to Favorite" forState:UIControlStateNormal];
        [cell.addToFavrtButton addTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 171;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return headerSegmentControl;
}

#pragma mark - event handlers

-(void) didTapFiterItem
{
    filterView = [[[NSBundle mainBundle] loadNibNamed:@"FilterView" owner:self options:nil] objectAtIndex:0];
    
    filterView.layer1Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    filterView.layer2Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    filterView.sortByButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    filterView.filterButton.layer.cornerRadius = 10.0f;
    filterView.cancelButton.layer.cornerRadius = 10.0f;
    filterView.cancelButton.layer.borderColor = GAThemeColor.CGColor;
    filterView.cancelButton.layer.borderWidth = 1.0f;

    [filterView.layer1Button addTarget:self action:@selector(didTapLayer1Button) forControlEvents:UIControlEventTouchUpInside];
    [filterView.layer2Button addTarget:self action:@selector(didTapLayer2Button) forControlEvents:UIControlEventTouchUpInside];
    [filterView.sortByButton addTarget:self action:@selector(didTapSortByButton) forControlEvents:UIControlEventTouchUpInside];
    [filterView.cancelButton addTarget:self action:@selector(didTapCancelButton) forControlEvents:UIControlEventTouchUpInside];
    [filterView.filterButton addTarget:self action:@selector(didTapFilterButton) forControlEvents:UIControlEventTouchUpInside];
    
    popupController = [[CNPPopupController alloc] initWithContents:@[filterView]];
    
    popupController.theme = [CNPPopupTheme defaultTheme];
    
    CNPPopupTheme *theme = [[CNPPopupTheme alloc] init];
    
    theme.presentationStyle = CNPPopupPresentationStyleFadeIn;
    theme.shouldDismissOnBackgroundTouch = YES;
    theme.backgroundColor = [UIColor whiteColor];
    theme.popupStyle = CNPPopupStyleCentered;
    theme.maskType = CNPPopupMaskTypeDimmed;
    theme.maxPopupWidth = self.view.frame.size.width-10;
    theme.dismissesOppositeDirection = YES;
    theme.animationDuration = 0.5f;
    theme.contentVerticalPadding = 5.0f;
    theme.cornerRadius = 10.0f;
    theme.movesAboveKeyboard = YES;
    
    
    popupController.theme = theme;
    
    
    [popupController presentPopupControllerAnimated:YES];
    
}

-(void) didTapSendIquiryButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self sendInquiryToPropertyId:[currentProperty objectForKey:@"ID"] targetButton:(UIButton *) sender];
    
}

-(void) didTapCancelRequestButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self cancelRequestToProprtyID:[currentProperty objectForKey:@"ID"] targetButton:(UIButton *) sender];
    
}

-(void) didTapAddFavouriteButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self addToFavouriteWithPropertyId:[currentProperty objectForKey:@"ID"] targetButton:(UIButton*)sender];
}

-(void) didTapDeleteFavouriteButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self deleteFromFavouriteWithPropertyId:[currentProperty objectForKey:@"ID"] targetButton:(UIButton*) sender];
}

-(void) didChangeSegmentControlValue
{
    [self.tableView reloadData];
}

-(void) didTapLayer1Button
{
 
    ActionSheetStringPicker *picker = [ActionSheetStringPicker showPickerWithTitle:@"Select Layer 1" rows:layer1Array initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
    
        filterView.layer1Field.text = [layer1Array objectAtIndex:selectedIndex];
        layer1String = [layer1Array objectAtIndex:selectedIndex];
        
    } cancelBlock:nil origin:self.view];
    
}

-(void) didTapLayer2Button
{
    ActionSheetStringPicker *picker = [ActionSheetStringPicker showPickerWithTitle:@"Select Layer 2" rows:layer2Array initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        filterView.layer2Field.text = [layer2Array objectAtIndex:selectedIndex];
        layer2String = [layer2Array objectAtIndex:selectedIndex];
        
    } cancelBlock:nil origin:self.view];
    
}

-(void) didTapSortByButton
{
    ActionSheetStringPicker *picker = [ActionSheetStringPicker showPickerWithTitle:@"Select Sorting Option" rows:@[@"Low To High", @"High to Low"] initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        filterView.sortByField.text = [@[@"Low To High", @"High to Low"] objectAtIndex:selectedIndex];
        sortString = [NSString stringWithFormat:@"%li",selectedIndex];
        
    } cancelBlock:nil origin:self.view];
}

-(void) didTapFilterButton
{
    [popupController dismissPopupControllerAnimated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL;
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    serviceURL = [NSString stringWithFormat:@"%@?userid=%@&&token=%@",GAFilterSearchServiceURL,[userInfo objectForKey:GALoginInfoLoginIdKey],[userInfo objectForKey:GALoginInfoLoginTokenKey]];
    
    NSDictionary *params = @{
                             @"layer1id" : layer1String? layer1String : @"",
                             @"layer2id" : layer2String? layer2String : @"",
                             @"pricefrom" : filterView.priceFromField.text,
                             @"priceto" : filterView.priceToField.text,
                             @"areafrom" : filterView.areaFromField.text,
                             @"areato" : filterView.areaToField.text,
                             @"sort" : sortString? sortString : @""
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager POST:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            NSDictionary *properties = [json objectForKey:@"Properties"];
            
            saleArray = [properties objectForKey:@"Sale"];
            wantedArray = [properties objectForKey:@"Wanted"];
            
            [hud hideAnimated:YES];
            
            [self.tableView reloadData];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

-(void) didTapCancelButton
{
    [popupController dismissPopupControllerAnimated:YES];
}

@end
