//
//  GASearchResultsViewController.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 25/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GASearchResultsViewController : UIViewController

@property (strong , nonatomic) NSDictionary *params;

@end
