//
//  GASearchPropertiesViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 5/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import "GASearchPropertiesViewController.h"
#import "GASearchResultsViewController.h"

#import "GASearchMainTableViewCell.h"
#import "GASearchSubTableViewCell.h"

#define kSuperCell @"kSuperCell"
#define kSubCell @"kSubCell"


#define kCellTypeKey @"kCellTypeKey"
#define kCellDataKey @"kCellDataKey"



#pragma mark - SearchCell Class


@interface SearchCell : NSObject

@property (strong , nonatomic) NSDictionary *data;
@property (strong , nonatomic) NSString *type;
@property (nonatomic) BOOL isSelected;

-(instancetype) initWithData : (NSDictionary *) data CellType : (NSString *) type;

@end

@implementation SearchCell

-(instancetype)initWithData:(NSDictionary *)data CellType:(NSString *)type
{
    self.data = data;
    self.type = type;
    self.isSelected = NO;
    
    return self;
}

@end




@interface GASearchPropertiesViewController () <UITableViewDelegate , UITableViewDataSource>
{
    BOOL isSelected;
    UIBarButtonItem *searchItem;
    
    NSMutableArray *cellArray;
    NSMutableArray *selectedArray;
    
    NSArray *schemesArray;
    NSArray *subSchemesArray;
    
    
}
@end

@implementation GASearchPropertiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.navigationItem.title = @"Select Subscheme";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
     searchItem = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStyleDone target:self action:@selector(didTapSearchItem)];
    searchItem.enabled = NO;
    self.navigationItem.rightBarButtonItem = searchItem;
    
    selectedArray = [[NSMutableArray alloc] init];
    
    [self callService];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - web service methods
-(void) callService
{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GASearchListServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"subschemes": selectedArray
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        cellArray = [[NSMutableArray alloc] init];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            NSArray *schemes = [json objectForKey:@"Schemes"];
            
            for (NSDictionary *dict in schemes)
            {
                [cellArray addObject:[[SearchCell alloc] initWithData:dict CellType:kSuperCell]];
            }
            
            [hud hideAnimated:YES];
            [self.tableView reloadData];
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cellArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SearchCell *currentCell = [cellArray objectAtIndex:indexPath.row];
        
    if ([currentCell.type isEqualToString:kSuperCell])
    {
        GASearchMainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GASearchMainTableViewCell"];
        
        if(currentCell.isSelected)
        {
            cell.checkImageView.layer.borderWidth = 0.0f;
            cell.checkImageView.backgroundColor = GAThemeColor;
            [cell.checkImageView setImage:[UIImage imageNamed:@"check_white.png"] forState:UIControlStateNormal];
        }
        else
        {
            cell.checkImageView.backgroundColor = [UIColor whiteColor];
            cell.checkImageView.layer.cornerRadius = cell.checkImageView.frame.size.width/2;
            cell.checkImageView.clipsToBounds = YES;
            cell.checkImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            cell.checkImageView.layer.borderWidth = 1.0f;
            [cell.checkImageView setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];

        }
        
        cell.nameLabel.text = [currentCell.data objectForKey:@"Name"];
        
        return cell;
        
    }
    else if ([currentCell.type isEqualToString:kSubCell])
    {
        GASearchSubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GASearchSubTableViewCell"];
        
        if(currentCell.isSelected)
        {
            cell.checkImageView.layer.borderWidth = 0.0f;
            cell.checkImageView.backgroundColor = GAThemeColor;
            [cell.checkImageView setImage:[UIImage imageNamed:@"check_white.png"] forState:UIControlStateNormal];
        }
        else
        {
            cell.checkImageView.backgroundColor = [UIColor whiteColor];
            cell.checkImageView.layer.cornerRadius = cell.checkImageView.frame.size.width/2;
            cell.checkImageView.clipsToBounds = YES;
            cell.checkImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
            cell.checkImageView.layer.borderWidth = 1.0f;
            [cell.checkImageView setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        }
        
         cell.nameLabel.text = [currentCell.data objectForKey:@"Name"];
        
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    SearchCell *currentCell = [cellArray objectAtIndex:indexPath.row];
    
    if([currentCell.type isEqualToString:kSuperCell])
    {
        if(currentCell.isSelected)
        {
            currentCell.isSelected = NO;
            GASearchMainTableViewCell *cell = (GASearchMainTableViewCell*) [tableView  cellForRowAtIndexPath:indexPath];
          
            UIButton *button = (UIButton *) [cell viewWithTag:1];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            button.layer.borderWidth = 1.0f;
            
            NSArray *subschemes = [currentCell.data objectForKey:@"Subschemes"];

            int count = 0;
            NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];

            for (long i = indexPath.row + 1; i<= indexPath.row+subschemes.count; i++)
            {
                count++;
                [cellArray removeObjectAtIndex:indexPath.row+1];
                [indexPathArray addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            
            [self.tableView beginUpdates];
            [self.tableView deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];

        }
        else
        {
            GASearchMainTableViewCell *cell = (GASearchMainTableViewCell*) [tableView  cellForRowAtIndexPath:indexPath];
            
            UIButton *button = (UIButton *) [cell viewWithTag:1];
            [button setBackgroundColor:GAThemeColor];
            [button setImage:[UIImage imageNamed:@"check_white.png"] forState:UIControlStateNormal];
            button.layer.borderWidth = 0.0f;
            currentCell.isSelected = YES;
            
            NSArray *subschemes = [currentCell.data objectForKey:@"Subschemes"];
            
            int count = 0;
            NSMutableArray *indexPathArray = [[NSMutableArray alloc] init];
         
            for (long i = indexPath.row + 1; i<= indexPath.row+subschemes.count; i++)
            {
                NSDictionary *dict = [subschemes objectAtIndex:count];
                count++;
                [cellArray insertObject:[[SearchCell alloc] initWithData:dict CellType:kSubCell] atIndex:i];
                [indexPathArray addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            
            
            [self.tableView beginUpdates];
            [self.tableView insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
            
        }
    }
    else if ([currentCell.type isEqualToString:kSubCell])
    {
        if(currentCell.isSelected)
        {
            currentCell.isSelected = NO;
            
            GASearchSubTableViewCell *cell = (GASearchSubTableViewCell*) [tableView  cellForRowAtIndexPath:indexPath];
            UIButton *button = (UIButton *) [cell viewWithTag:1];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button setImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
            button.layer.borderWidth = 1.0f;
            
            [selectedArray removeObject:[currentCell.data objectForKey:@"ID"]];
            
            if(selectedArray.count == 0)
                searchItem.enabled = NO;
        }
        else
        {
            currentCell.isSelected = YES;
            
            GASearchSubTableViewCell *cell = (GASearchSubTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
            UIButton *button = (UIButton *) [cell viewWithTag:1];
            [button setBackgroundColor:GAThemeColor];
            [button setImage:[UIImage imageNamed:@"check_white.png"] forState:UIControlStateNormal];
            button.layer.borderWidth = 0.0f;
            
            [selectedArray addObject:[currentCell.data objectForKey:@"ID"]];
            searchItem.enabled = YES;
            
            return;
        }
    }
    
//    self.tableView.userInteractionEnabled = NO;
//    [UIView animateWithDuration:0.0 delay:3 options:UIViewAnimationOptionTransitionNone animations:^{
//        [self.tableView reloadData];
//    } completion:^(BOOL finished) {
//        self.tableView.userInteractionEnabled = YES;
//
//    }];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    
    titleLabel.text = @"Select the sub schemes you want to search";
    titleLabel.backgroundColor = [UIColor whiteColor];
    titleLabel.textColor = GAThemeColor;
    titleLabel.font = GAMediumFont;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    
    return titleLabel;
}


#pragma mark - event handlers

-(void) didTapSearchItem
{
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [userInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [userInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"subschemes" : selectedArray
                             };
    
    GASearchResultsViewController *resultsVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GASearchResultsViewController"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:self action:nil];
    resultsVC.params = params;
    
    [self.navigationController pushViewController:resultsVC animated:YES];
}

@end
