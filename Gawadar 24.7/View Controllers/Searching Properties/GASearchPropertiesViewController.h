//
//  GASearchPropertiesViewController.h
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 5/8/17.
//  Copyright © 2017 HomeMade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GASearchPropertiesViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
