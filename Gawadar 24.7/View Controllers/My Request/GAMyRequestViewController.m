//
//  GAMyRequestViewController.m
//  Gawadar 24.7
//
//  Created by Wasi Tariq on 04/01/2018.
//  Copyright © 2018 HomeMade. All rights reserved.
//


#import "GAMyRequestViewController.h"

#import "GAHomeTableViewCell.h"

#import "HCSStarRatingView.h"

@interface GAMyRequestViewController ()<UITableViewDelegate , UITableViewDataSource>
{
    UISegmentedControl *headerSegmentControl;
    
    NSMutableArray *saleArray;
    NSMutableArray *wantedArray;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GAMyRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.navigationBar.barTintColor = GAThemeColor;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.navigationItem.title = @"My Requests";
    
    // header segment control
    headerSegmentControl = [[UISegmentedControl alloc] initWithItems:@[@"Sale",@"Wanted"]];
    headerSegmentControl.tintColor = GAThemeColor;
    headerSegmentControl.selectedSegmentIndex = 0;
    headerSegmentControl.backgroundColor = [UIColor whiteColor];
    [headerSegmentControl addTarget:self action:@selector(didChangeSegmentControlValue) forControlEvents:UIControlEventValueChanged];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self callService];
}

#pragma mark - Web service methods

-(void) callService
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GAMyRequestServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey]
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            NSDictionary *properties = [json objectForKey:@"Properties"];
            
            saleArray = ((NSArray *)[properties objectForKey:@"Sale"]).mutableCopy;
            wantedArray = ((NSArray *)[properties objectForKey:@"Wanted"]).mutableCopy;
            
            [hud hideAnimated:YES];
            
            [self.tableView reloadData];
        }
        
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
}

-(void) addToFavouriteWithPropertyId : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GAAddToFavouriteServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property has been added to favourite list" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Delete Favorite" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapDeleteFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in adding this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in adding this property to list. Try again later." iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}

-(void) deleteFromFavouriteWithPropertyId : (NSString *) propertyID targetButton : (UIButton *) button
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *serviceURL = GADeleteFromFavouriteServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            [ISMessages showCardAlertWithTitle:@"Success" message:@"Property has been deleted from favourite list" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [button setTitle:@"Add to Favorite" forState:UIControlStateNormal];
            [button removeTarget:self action:@selector(didTapDeleteFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
            
            [hud hideAnimated:YES];
        }
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Erorr in adding this property" iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in adding this property to list. Try again later." iconImage:nil duration:5.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    long count = (headerSegmentControl.selectedSegmentIndex == 0)? [saleArray count] : [wantedArray count];
    
    return count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GAHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GAHomeTableViewCell"];
    
    
    if(indexPath.row%2 == 0)
        cell.contentView.backgroundColor = GAEvenCellColor;
    else
        cell.contentView.backgroundColor = GAOddCellColor;
    HCSStarRatingView *ratingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-100, cell.priceLabel.frame.origin.y-5,80, 40)];
    ratingView.tintColor = GAThemeColor;
    ratingView.minimumValue = 0;
    ratingView.maximumValue = 5;
    ratingView.backgroundColor = [UIColor clearColor];
    [cell.contentView addSubview:ratingView];
    
    
    cell.priceLabel.textColor = GAThemeColor;
    cell.propertyImageView.layer.cornerRadius = cell.propertyImageView.frame.size.height/2;
    cell.propertyImageView.clipsToBounds = YES;
    
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    [cell.sendInquiryButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    
    
    cell.addToFavrtButton.layer.borderColor = GAThemeColor.CGColor;
    [cell.addToFavrtButton setTitleColor:GAThemeColor forState:UIControlStateNormal];
    cell.addToFavrtButton.layer.borderWidth = 1.0f;
    cell.addToFavrtButton.layer.cornerRadius = 5.0f;
    
    cell.sendInquiryButton.backgroundColor = GAThemeColor;
    cell.sendInquiryButton.layer.cornerRadius = 5.0f;
    [cell.sendInquiryButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    // data populating
    NSArray *array = (headerSegmentControl.selectedSegmentIndex == 0)? saleArray : wantedArray;
    
    NSDictionary *currentProperty = [array objectAtIndex:indexPath.row];
    
    ratingView.value = [[currentProperty objectForKey:@"Rating"] floatValue];
    cell.priceLabel.text = [currentProperty objectForKey:@"Price"];
    cell.areaLabel.text = [currentProperty objectForKey:@"Area"];
    cell.sizeLabel.text = [currentProperty objectForKey:@"Area"];
    cell.phaseLabel.text = [currentProperty objectForKey:@"PlotType"];
    cell.schemeLabel.text = [currentProperty objectForKey:@"SchemeName"];
    cell.subschemeLabel.text = [currentProperty objectForKey:@"SubSchemeName"];
    cell.phaseLabel.text = [currentProperty objectForKey:@"Layer1Name"];
    [cell.propertyImageView sd_setImageWithURL:[currentProperty objectForKey:@"Image"] placeholderImage:[UIImage imageNamed:@""]];
    
    
    cell.sendInquiryButton.tag = indexPath.row;
    cell.addToFavrtButton.tag = indexPath.row ;
    
    
    [cell.sendInquiryButton addTarget:self action:@selector(didTapRemovePropertyButton:) forControlEvents:UIControlEventTouchUpInside];
    [cell.addToFavrtButton addTarget:self action:@selector(didTapRemovePropertyButton:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.addToFavrtButton setTitle:@"Delete Property" forState:UIControlStateNormal];
    
    // new data
    cell.line1.text = [currentProperty objectForKey:@"Line1Text"];
    cell.line2.text = [currentProperty objectForKey:@"Line2Text"];
    cell.line3.text = [currentProperty objectForKey:@"Line3Text"];
    cell.line4.text = [currentProperty objectForKey:@"Line4Text"];
    cell.line5.text = [currentProperty objectForKey:@"Line5Text"];
    cell.line6.text = [currentProperty objectForKey:@"Line6Text"];
    
    
    cell.icon1.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@.png",[(NSString *)[currentProperty objectForKey:@"Line1Icon"] lowercaseString]]
                        ];
    cell.icon2.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line2Icon"] lowercaseString]]
                        ];
    cell.icon3.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line3Icon"] lowercaseString]]
                        ];
    cell.icon4.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line4Icon"] lowercaseString]]
                        ];
    cell.icon5.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line5Icon"] lowercaseString]]
                        ];
    cell.icon6.image = [UIImage imageNamed:
                        [NSString stringWithFormat:@"%@",[(NSString *)[currentProperty objectForKey:@"Line6Icon"] lowercaseString]]
                        ];
    
    if([[currentProperty objectForKey:@"IsFavorite"] isEqualToString:@"true"])
    {
        [cell.addToFavrtButton setTitle:@"Delete Favorite" forState:UIControlStateNormal];
        [cell.addToFavrtButton addTarget:self action:@selector(didTapDeleteFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [cell.addToFavrtButton setTitle:@"Add to Favorite" forState:UIControlStateNormal];
        [cell.addToFavrtButton addTarget:self action:@selector(didTapAddFavouriteButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 171;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return headerSegmentControl;
}

#pragma mark - event hanlders
-(void) didTapRemovePropertyButton : (id) sender
{
    UIButton *button = (UIButton *) sender;
    
    long tag = button.tag;
    
    NSMutableArray *array = (headerSegmentControl.selectedSegmentIndex == 0)? saleArray : wantedArray;
    
    NSString *propertyID = [(NSDictionary *)[array objectAtIndex:tag] objectForKey:@"ID"];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey])
    {
        GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
        
        UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
        
        window.rootViewController = loginVC;
    }
    
    NSString *serviceURL = GARemovePropertyServiceURL;
    
    NSDictionary *loginInfo = [[NSUserDefaults standardUserDefaults] objectForKey:GALoginInformationDictionaryKey];
    
    NSDictionary *params = @{
                             @"userid" : [loginInfo objectForKey:GALoginInfoLoginIdKey],
                             @"token" : [loginInfo objectForKey:GALoginInfoLoginTokenKey],
                             @"propertyid" : propertyID
                             };
    
    GAWebServiceSingleton *manager = [GAWebServiceSingleton getInstance];
    
    [manager GET:serviceURL parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *json = [GAUtilities getDictionaryFromData:responseObject];
        
        if([[json objectForKey:@"ErrorCode"] isEqualToString:@"000"])
        {
            
            [hud hideAnimated:YES];
            
            [ISMessages showCardAlertWithTitle:@"Success" message:@"property deleted successfully" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeSuccess alertPosition:ISAlertPositionTop];
            
            [array removeObjectAtIndex:tag];
            
            [self.tableView beginUpdates];
            
            [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:tag inSection:0]] withRowAnimation:UITableViewRowAnimationRight];
            
            [self.tableView endUpdates];
            
            
            [UIView animateWithDuration:0.0 delay:1.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
                [self.tableView reloadData];
            } completion:nil];
            
            
        }
        else if ([[json objectForKey:@"ErrorCode"] isEqualToString:@"802"])
        {
            [hud hideAnimated:YES];
            
            [ISMessages showCardAlertWithTitle:@"Session Expired!" message:@"Login Again to proceed" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
            
            GALoginViewController *loginVC = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"GALoginViewController"];
            
            UIWindow *window = [[[UIApplication sharedApplication] delegate ] window];
            window.rootViewController = loginVC;
        }
        
        else
        {
            [hud hideAnimated:YES];
            [ISMessages showCardAlertWithTitle:@"Error" message:@"Error in loading properties" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        }
        
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [hud hideAnimated:YES];
        [ISMessages showCardAlertWithTitle:@"Error" message:@"There is some error in loading the porperties. Try again later" iconImage:nil duration:1.0f hideOnSwipe:YES hideOnTap:YES alertType:ISAlertTypeError alertPosition:ISAlertPositionTop];
        
    }];
    
}

-(void) didChangeSegmentControlValue
{
    [self.tableView reloadData];
}
-(void) didTapAddFavouriteButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self addToFavouriteWithPropertyId:[currentProperty objectForKey:@"ID"] targetButton:(UIButton*)sender];
}

-(void) didTapDeleteFavouriteButton : (id) sender
{
    long tag = ((UIButton *) sender).tag;
    
    NSDictionary *currentProperty;
    
    if(tag>=0)
    {
        currentProperty = [saleArray objectAtIndex:tag];
    }
    else
    {
        currentProperty = [wantedArray objectAtIndex:tag * -1];
    }
    
    [self deleteFromFavouriteWithPropertyId:[currentProperty objectForKey:@"ID"] targetButton:(UIButton*) sender];
}

@end
